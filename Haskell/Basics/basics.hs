-- module Basics where

main :: IO ()

main = 
  let message = "Hello, World from Basics module!"
  in putStrLn message

-- sumSquare x y = x ^ 2 + y ^ 2
-- ifXnegative x = if x > 0 then 1 else (-1)
-- f x y = (ifXnegative x) + y
-- max5 x = max 5 x
-- max5' = max 5

-- hello = 'H' : "ello"
-- helloWorld = hello ++ ", World!"

-- -- Рекурсия вместо циклов
-- factorial n = if n == 0 then 1 else n * factorial (n - 1)

-- -- Сопоставление с образцом (Patern matching)
-- factorial' 0 = 1
-- factorial' n = n * factorial' (n - 1)

-- -- Errors
-- factorial'' 0 = 1
-- factorial'' n = 
--   if n < 0 
--     then error "arg must be >= 0" 
--     else n * factorial'' (n - 1)
-- factorial'' n = 
--   if n < 0 
--     then undefined 
--     else n * factorial'' (n - 1)

-- -- Охранные выражения (Guard conditions)
-- factorial''' 0 = 1
-- factorial''' n | n < 0 = error "arg must be >= 0"
--                | n > 0 = n * factorial''' (n - 1)

factorial4 :: Integer -> Integer
factorial4 n 
  | n == 0 = 1
  | n > 0 = n * factorial4 (n - 1)
  | otherwise = error "arg must be >= 0"

-- factorial5 n | n >= 0    = helper 1 n
--              | otherwise = error "arg must be >= 0"
-- helper acc 0 = acc
-- helper acc n = helper (acc * n) (n - 1)

