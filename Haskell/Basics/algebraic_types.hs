-- data Shape = Circle Float Float Float | Rectangle Float Float Float Float
data Shape = Circle Float Float Float | Rectangle Float Float Float Float deriving (Show)  -- typeclass

surface :: Shape -> Float
surface (Circle _ _ r) = pi * r ^ 2
surface (Rectangle x1 y1 x2 y2) = (abs $ x2 - x1) * (abs $ y2 - y1)

data Person = Person 
    { firstName :: String
    , lastName :: String
    , age :: Int
    , height :: Float
    , phoneNumber :: String
    , flavor :: String
    } deriving (Show)

data Day
    = Monday
    | Tuesday
    | Wednesday
    | Thursday
    | Friday
    | Saturday
    | Sunday deriving (Eq, Ord, Show, Read, Bounded, Enum)

phoneBook :: [(String, String)]
phoneBook =
    [("John", "111-2222")
    ,("Max", "333-4444")
    ,("Alex", "555-7777")
    ]

type PhoneNumber = String
type Name = String
type PhoneBook = [(Name, PhoneNumber)]

lucky :: Int -> String
lucky 7 = "Lucky number 7!"
lucky x = "Sorry, you'll be lucky next time!"
