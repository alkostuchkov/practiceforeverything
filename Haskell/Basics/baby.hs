doubleMe :: Integer -> Integer
doubleMe x = x + x
-- doubleUs x y = x*2 + y*2

doubleUs :: Integer -> Integer -> Integer
doubleUs x y = doubleMe x + doubleMe y

-- if .. then .. else
sayGreeting :: String -> String
sayGreeting timeDay =
  if timeDay == "morning"
    then "Good morning!"
    else if timeDay == "afternoon"
      then "Good afternoon!"
      else if timeDay == "everning" 
        then "Good everning!"
        else "Unknown time-day."

sayGreeting' :: String -> String
sayGreeting' timeDay  
  | timeDay == "morning" = "Good morning!"
  | timeDay == "afternoon" = "Good afternoon!"
  | timeDay == "everning" = "Good everning!"
  | otherwise = "Unknown time-day."

-- case .. of ..
sayGreeting'' :: String -> String
sayGreeting'' timeDay =
  case timeDay of
    "morning" -> "Good morning!"
    "afternoon" -> "Good afternoon!"
    "everning" -> "Good everning!"
    _ -> "Unknown time-day."

sayGreeting''' :: String -> String
sayGreeting''' "morning" = "Good morning!"
sayGreeting''' "afternoon" = "Good afternoon!"
sayGreeting''' "everning" = "Good everning!"
sayGreeting''' _ = "Unknown time-day."

boomBangs :: [Integer] -> [String]
boomBangs xs = [if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]

boomBangs' :: [Integer] -> [String]
boomBangs' xs = [if odd x then "BOOM!" else "BANG!" | x <- xs]

isLocalhost :: String -> Bool
isLocalhost ip = 
  if ip == "127.0.0.1" || ip == "0.0.0.0"
    then True
    else False

bmi :: Double -> Double -> String
bmi weight height
  | weight / (height ^ 2) <= 18.5 = "You're skinny!"
  | weight / (height ^ 2) <= 25.0 = "You're normal!" 
  | weight / (height ^ 2) <= 30.0 = "You're fat!" 
  | otherwise                     = "I don't know who you are!" 

bmi' :: (RealFloat a) => a -> a -> String
bmi' weight height
  | result <= skinny = "You're skinny!"
  | result <= normal = "You're normal!" 
  | result <= fat    = "You're fat!" 
  | otherwise        = "I don't know who you are!" 
  where result = weight / (height ^ 2)
        (skinny, normal, fat) = (18.5, 25.0, 30.0)

cylinder :: (RealFloat a) => a -> a -> a
cylinder radius height =
  let sideArea = 2 * pi * radius * height
      topArea = pi * radius^2
  in sideArea + topArea*2

-- checkMultiIf :: (Ord a, Num a) => a -> String
checkMultiIf :: (Ord a, Num a) => a -> IO ()
checkMultiIf x =
  if x > 0 then do
    putStrLn "First line: x > 0"
    putStrLn "Second line: x > 0"
  else if x < 0 then do
    putStrLn "First line: x < 0"
    putStrLn "Second line: x < 0"
  else 
    putStrLn "x == 0"