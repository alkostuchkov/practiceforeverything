-- main :: IO ()
-- main = 
--   let
--     a = 1
--     b = 2
--     c = 1
--     d = b * b - 4 * a * c :: Float
--   in 
--     if d < 0 then
--       putStrLn "No resolves"
--     else
--       let
--         x1 = (-b - sqrt d) / ( 2 + a)
--         x2 = (-b + sqrt d) / ( 2 + a)
--       in
--         print (x1, x2)

-- main :: IO ()
-- main = 
--   if d < 0 then
--     putStrLn "No resolves"
--   else
--     print (x1, x2)

--   where
--     a = 1
--     b = 2
--     c = 1
--     d = b * b - 4 * a * c :: Float
--     x1 = (-b - sqrt d) / ( 2 + a)
--     x2 = (-b + sqrt d) / ( 2 + a)

-- main :: IO ()
-- main = 
--   if d < 0 then
--     putStrLn "No resolves"
--   else
--     let
--       x1 = (-b - sqrt d) / ( 2 + a)
--       x2 = (-b + sqrt d) / ( 2 + a)
--     in
--       print (x1, x2)

--   where
--     a = 1
--     b = 2
--     c = 1
--     d = b * b - 4 * a * c :: Float

-- main :: IO ()
-- main
--   | a == 0    = error "a can't be equal 0!"
--   | d < 0     = putStrLn "No resolves"
--   | otherwise = print (x1, x2)
--   where
--     a = 1
--     b = 2
--     c = 1
--     d = b * b - 4 * a * c :: Float
--     x1 = (-b - sqrt d) / ( 2 * a)
--     x2 = (-b + sqrt d) / ( 2 * a)

main :: IO ()
main =
  -- print (solveSquare a b c)
  print $ solveSquare a b c
  where
    a = 1
    b = 2
    c = 1

solveSquare :: Double -> Double -> Double -> [Double]
solveSquare a b c
  | a == 0 && b == 0 = []
  | a == 0           = [-c / b]
  | d < 0            = []
  | otherwise        = [op (-b) (sqrt d) / ( 2 * a) | op <- [(-), (+)]]
  -- | otherwise        = [(-b `op` sqrt d) / ( 2 * a) | op <- [(-), (+)]]
  where
    d = b * b - 4 * a * c