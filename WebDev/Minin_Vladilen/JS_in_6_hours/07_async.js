//Event loop
// const timeoutId = setTimeout(() => console.log("After timeout"), 2500);
// console.log("During timeout");
// clearTimeout(timeoutId);
//
// const intervalId = setInterval(() => console.log("After interval"), 1000);
// // clearInterval(intervalId);

// const delay = (callback, wait=1000) => {
//   setTimeout(callback, wait);
// }
//
// delay(() => console.log("After delay"), 2000);

const delay = (wait = 1000) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // resolve();
      reject("Something goes wrong!");
    }, wait);
  });
};

// delay(2500)
//   .then(() => {
//     console.log("After delay");
//   })
//   .catch((err) => console.error("Error:", err))
//   .finally(() => console.log("Finally"));

const getData = () => new Promise((resolve) => resolve([1, 1, 2, 3, 5, 8, 13]));
// getData().then((data) => console.log(data));

async function asyncExample() {
  try {
    await delay(3000);
    const data = await getData();
    console.log("Data:", data);
  } catch (e) {
    console.log(e);
  } finally {
    console.log("Finally");
  }
}

asyncExample();
