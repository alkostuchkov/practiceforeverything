const cars = ["Mazda", "Ford", "BMW", "Mersedes"];

// cars.push('Reno');
// cars.unshift('Volga');
// console.log(cars);
// cars.shift();
// console.log(cars);
// cars.pop();
// console.log(cars);
//
// const index = cars.indexOf('BMW');
// cars[index] = 'Volga';
// console.log(cars);
// console.log(cars.includes("Ford"));
// const upperCaseCars = cars.map(car => car.toUpperCase());
// console.log(upperCaseCars);
// cars.forEach(car => console.log(car));

const fib = [1, 1, 2, 3, 5, 8, 13];
// const square = num => num ** 2;
// // const result = fib.map(square).map(Math.sqrt);
// const result = fib.map(square);
// console.log(result);
// const resultFiltered = result.filter(num => num > 50);
// console.log(resultFiltered);

const people = [
  {name: "Alex", budget: 7700},
  {name: "John", budget: 4500},
  {name: "Maxim", budget: 3600},
];
const allBudget = people.filter((person) => person.budget > 4000).
  reduce((accum, person) => {
    accum += person.budget;
    return accum;
  }, 0);
console.log(allBudget);

// const i = people.findIndex(person => {
//   // if (person.budget >= 4500) {
//   //   console.log(person);
//   // }
//   return person.budget >= 4500;
// });
// console.log(i);
// console.log(people[i]);

// const person = people.find(person => person.budget === 4500);
// console.log(person);
//
// for (const person of people) {
//   if (person.budget >= 4500) {
//     console.log(person);
//   }
// }

// let v;
// if (v === undefined) {
//   console.log("isUndefined");
// } else {
//   console.log("isNotUndefined");
// }
