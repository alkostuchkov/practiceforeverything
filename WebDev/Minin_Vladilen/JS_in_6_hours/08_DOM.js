const title = document.getElementById("title");
// const head_2 = document.getElementsByTagName("h2")[0];
const head_2 = document.querySelector("h2");
const h2List = document.querySelectorAll("h2");
// console.log(head_2.nextElementSibling);
// console.dir(title);
console.log(h2List);

const link = h2List[1].querySelector("a");

link.addEventListener("click", (event) => {
  event.preventDefault();
  console.log("onclick", event);
  console.log("event.target.getAttribute", event.target.getAttribute("href"));
  const url = event.target.getAttribute("href");
  window.location = url;
});

function changeNode(
  node,
  fontSize,
  text = "Changed from JavaScript!",
  color = "red"
) {
  node.innerText = text;
  node.style.color = color;
  node.style.textAlign = "center";
  node.style.background = "black";
  node.style.padding = "2rem";
  node.style.width = "100%";
  node.style.display = "block";
  if (fontSize) {
    node.style.fontSize = fontSize;
  }
}

setTimeout(() => changeNode(title, "", "JavaScript", "white"), 3000);
setTimeout(() => changeNode(head_2, "", "is"), 5000);
// setTimeout(() => changeNode(head_2.nextElementSibling), 5000);
setTimeout(() => changeNode(link, "2rem", "AMAZING!!!", "yellow"), 7000);

function getRandomColor() {
  const red = +(Math.random() * 255).toFixed(0);
  const green = +(Math.random() * 255).toFixed(0);
  const blue = +(Math.random() * 255).toFixed(0);
  return [red, green, blue];
}

title.onclick = () => {
  let colors = getRandomColor();
  title.style.color = `rgb(${colors[0]}, ${colors[1]}, ${colors[2]})`;
  colors = getRandomColor();
  title.style.background = `rgb(${colors[0]}, ${colors[1]}, ${colors[2]})`;
};

head_2.addEventListener("dblclick", () => {
  let colors = getRandomColor();
  head_2.style.color = `rgb(${colors[0]}, ${colors[1]}, ${colors[2]})`;
  colors = getRandomColor();
  head_2.style.background = `rgb(${colors[0]}, ${colors[1]}, ${colors[2]})`;
});
