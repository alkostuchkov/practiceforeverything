const person = {
  name: "Alexander",
  age: 20,
  isProgrammer: true,
  languages: ["en", "ru", "de"],
  "complex key": "Complex Value",
  ["key_" + (1 + 3)]: "Computed Value",
  greet() {
    console.log("Hello!");
  },
  info() {
    console.log(`Info about ${this.name}:`);
    console.log(`Age: ${this.age}`);
    console.log(`Is programmer: ${this.isProgrammer}`);
    console.log(`Languages: ${this.languages}`);
  }
};
// let key = "name";
// console.log(person[key]);
// key = "complex key";
// console.log(person[key]);
// key = "key_" + (1 + 3);
// console.log(person[key]);
// delete person[key];
// console.log(person);
//
// let {name, age: personAge = 10, languages} = person;
// console.log(name, personAge, languages);

// // Old method go through object's keys.
// for (let key in person) {
//   if (person.hasOwnProperty(key)) {
//     console.log(key, "=>", person[key]);
//   }
//   // Dangerous without checking hasOwnProperty(key) because loop for in goes into prototype...
//   // console.log(key, "=>", person[key]);
// }

// // New method.
// // const keys = Object.keys(person);
// // console.log(keys);
// Object.keys(person).forEach(key => console.log(key, "->", person[key]));
// person.info();

// Context
const logger = {
  keys() {
    console.log(`Object keys: ${Object.keys(this)}`);
  },

  keysAndValues() {
    // arrow function does not create its own context
    Object.keys(this).forEach(key => console.log(key, "->", this[key]));

    // function does create its own context and this in Object.keys(this) !== this[key]
    // Resolve 1:
    // const self = this;
    // Object.keys(this).forEach(function (key) {
    //   console.log(key, "->", self[key])
    // });

    // Resolve 2:
    // Object.keys(this).forEach(function (key) {
    //   console.log(key, "->", this[key])
    // }.bind(this));
  },

  withParams(top = false, between = false, bottom = false) {
    if (top) {
      console.log("----- Start -----");
    }
    Object.keys(this).forEach((key, index, array) => {
      console.log(key, "->", this[key]);
      if (between && index !== array.length - 1) {
        console.log("-----------------");
      }
    });
    if (bottom) {
      console.log("----- End -----");
    }
  }
};

// const bound = logger.keys.bind(person);
// bound();
// logger.keys.call(person);
// logger.keysAndValues.call(logger);
// logger.keysAndValues.call(person);
// logger.withParams.call(person, true, true, true);  //call takes non limit args
logger.withParams.apply(person, [true, true, true]);  //apply takes only two args
