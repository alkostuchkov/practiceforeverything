function createProgrammer(name) {
  const programmer = {
    name,
    code: () => console.log(`${name} is coding...`)
  }

  return {
    ...programmer
    // ...canCode(programmer)
  }
}

// function canCode({name}) {
//   return {
//     code: () => console.log(`${name} is coding...`)
//   }
// }

function canNodejs({name}) {
  return {
    nodejs: () => console.log(`${name} can Nodejs.`)
  }
}

function canAngular({name}) {
  return {
    angular: () => console.log(`${name} can Angular.`)
  }
}

function canReactAndVue({name}) {
  return {
    react: () => console.log(`${name} can React.`),
    vue: () => console.log(`${name} can Vue.`)
  }
}

function createFrontend(name) {
  const frontend = new createProgrammer(name)
  return {
    ...frontend,
    ...canAngular(frontend),
    ...canReactAndVue(frontend)
  }
}

function createBackend(name) {
  const backend = new createProgrammer(name)
  return {
    ...backend,
    ...canNodejs(backend)
  }
}

function createFullstack(name) {
  const fullstack = new createProgrammer(name)
  return {
    ...fullstack,
    ...canAngular(fullstack),
    ...canNodejs(fullstack)
  }
}

const programmer = new createProgrammer("Programmer")
// console.log(programmer)
programmer.code()

const frontend = new createFrontend("Frontend")
// console.log(frontend)
frontend.code()
frontend.angular()
frontend.react()
frontend.vue()

const backend = new createBackend("Backend")
// console.log(backend)
backend.code()
backend.nodejs()

const fullstack = new createFullstack("Fullstack")
// console.log(fullstack)
fullstack.code()
fullstack.angular()
fullstack.nodejs()
