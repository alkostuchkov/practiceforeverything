// +++ for await of ... +++

// +++ Objects +++
// const person = {
//   name: "John",
//   age: 14
// }

// console.log(Object.getOwnPropertyDescriptor(person, "name"))
// console.log(Object.getOwnPropertyDescriptors(person))

// console.log(Object.entries(["m", "a", "x"]))
// for (const [key, value] of Object.entries(person)) {
//   console.log(key, value)
// }

const bill1 = {
  bank: {
    name: "My bank",
    amount: {
      value: 1000,
      currency: "rub"
    }
  }
}

const bill2 = {
  bank: {}
}

function getBillValueFrom(bill) {
  // return bill && bill.bank && bill.bank.amount && bill.bank.amount.value
  //   ? bill.bank.amount.value
  //   : undefined
  return bill?.bank?.amount?.value
}
console.log(getBillValueFrom(bill1))
console.log(getBillValueFrom(bill2))
