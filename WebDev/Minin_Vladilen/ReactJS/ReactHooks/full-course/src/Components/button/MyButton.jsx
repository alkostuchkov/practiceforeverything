import React from "react"
import classes from "./MyButton.module.css"

function MyButton() {
  return <button className={classes.myBtn}>MyButton</button>
}

export default MyButton
