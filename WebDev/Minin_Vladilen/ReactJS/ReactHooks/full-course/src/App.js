import React, {useState} from "react"
// import MyButtonClass from "./Components/button/MyButtonClass"
// import MyButton from "./Components/button/MyButton"

function App() {
  const [counter, setCounter] = useState(0)

  function increaseCounter() {
    setCounter(counter + 1)
  }

  function decreaseCounter() {
    setCounter(counter - 1)
  }

  return (
    <div className="App">
      <h1>Counter: {counter}</h1>
      {/* <MyButtonClass /> */}
      {/* <MyButton /> */}
      <button onClick={increaseCounter}>Increase</button>
      <button onClick={decreaseCounter}>Decrease</button>
    </div>
  )
}

export default App
