const num1 = document.querySelector("#num1");
const num2 = document.querySelector("#num2");
const btnAdd = document.querySelector("#add");
const btnSub = document.querySelector("#sub");
const output = document.querySelector("#output");

function getInputValues() {
  const value1 = +num1.value;
  const value2 = parseInt(num2.value);

  return [value1, value2];
}

function displayResult(result) {
  output.closest(".card").style.display = "block";
  output.innerHTML = `Result is ${result}`;
  console.trace();
}

function handlerAdd() {
  const values = getInputValues();
  const result = values[0] + values[1];
  displayResult(result);
}

function handlerSub() {
  // debugger;

  const values = getInputValues();
  const result = values[0] - values[1];
  displayResult(result);
}

btnAdd.addEventListener("click", handlerAdd);
btnSub.addEventListener("click", handlerSub);

// const people = [
//   {id: 1, name: "John", age: 13},
//   {id: 2, name: "Maxim", age: 10},
//   {id: 3, name: "Vika", age: 33},
//   {id: 3, name: "Alex", age: 45}
// ];
// console.table(people);

console.time("timer");
const arr = [];
for (let i = 0; i < 100000; i++) {
  arr.push({el: i + 1});
}
console.timeEnd("timer");
