//Context
function log() {
  console.log(this);
}

const arrowLog = () => console.log(this);

const person = {
  name: "John",
  age: 20,
  log: log,
  arrowLog: arrowLog,
  delayLog: function () {
    const self = this;
    setTimeout(function () {
      // console.log(this.name, this.age);
      console.log(self.name, self.age);
    })
  },
  arrowDelayLog: function () {
    setTimeout(() => {
      console.log(this.name, this.age);
    })
  }
}

// person.log();
// person.arrowLog();
// person.delayLog();
person.arrowDelayLog();
