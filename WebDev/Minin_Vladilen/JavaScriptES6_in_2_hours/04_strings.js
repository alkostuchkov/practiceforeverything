const title = "Hello";
const isVisible = () => Math.random() > 0.5;

const template = `
  ${isVisible() ? "<p>Visible</p>" : ""}
  <h1 id="demo" style="color: red">${title}</h1>
`
console.log(template);

console.log(title.padStart(10, "="));
console.log(title.padEnd(10, "+"));
