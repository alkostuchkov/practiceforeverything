const cityField = "city";
const job = "Frontend Developer";

const person = {
  name: "Maxim",
  age: 18,
  // job: job,
  job,
  "country-live": "USA",
  ["bla-bla-bla " + cityField]: "New York",
  toString() {
    return Object.keys(this)
      .filter((key) => typeof this[key] !== "function")
      .map((key) => this[key])
      .join(" ");
  },
  greet() {
    console.log("Just a person's greet!");
  }
};

// Object.keys(person).forEach(key => console.log(key, "->", person[key]));
// console.log(person.toString());
// console.log(person);

//Methods
const first = {a: 1};
const second = {b: 2};

// console.log(Object.is(10, 10));
// // console.log(Object.assign(first, second));
// // console.log(first, second);
// console.log(Object.assign({}, first, second));
// console.log(first, second);

const third = Object.assign({}, first, second, {c: 3});
console.log(third);
console.log(Object.entries(third));
console.log(Object.keys(third));
console.log(Object.values(third));


// function fun() {
//   console.log(this);
// }
// const arrowFun = () => console.log(this);
// fun();
// arrowFun();