// //Rest
// function showArgs(a, b, ...args) {
//   console.log(args);
//   args.forEach(item => console.log(item));
// }
//
// showArgs(1, 2, 3, 4, 5, 6, 7);

// //Spread
// const arr = [1, 2, 3, 5, 8, 13];
// console.log(arr, "...", ...arr);
// console.log(Math.max(...arr));
// const fib = [1, ...arr, 21];
// console.log(fib);

// //Destruction
// const arr = [1, 2, 3, 5, 8, 13];
// // const [a, b = 42, ...c] = arr;
// // console.log(a, b, c);
// const [a,, c] = arr;
// console.log(a, c);

const person = {
  name: "John",
  // age: 20,
  sex: "M",
  greet() {
    console.log(`Hello, my name's ${this.name} and I'm ${this.age}.`);
  }
};

// const {name, age = 18, greet: greetPerson} = person;
// console.log(name, age);
// greetPerson.call(person);

const {name, ...rest} = person;
console.log(name, rest);

const newPerson = {...person, name: "Maxim", isProgrammer: false};
console.log(newPerson);