// // Primitive types: undefined, null, Boolean, Number (Infinity, NaN), BigInt, String, Symbol
// console.log(typeof undefined)
// console.log(typeof null)
// console.log(typeof true)
// console.log(typeof 1, typeof Infinity, typeof NaN)
// console.log(typeof 2n)
// console.log(typeof "hello")
// console.log(typeof Symbol("hello"))

// let value = () => console.log()
// value = undefined
// value = null

// let lang = "Python"
// if (lang) {
//   console.log("The best language is", lang)
// }

// // Truthy and Falsy
// const falsy_values = [undefined, null, false, 0, NaN, 0n, ""]
// for (const item of falsy_values) {
//   console.log(`${item} is ALWAYS ${!!item}`.padStart(30))
// }
// console.log()

// const remember_truthy = {
//   "[]": [],
//   "{}": {},
//   "function () {}": function () {}
// }
// for (const [key, value] of Object.entries(remember_truthy)) {
//   console.log(`${key} is ALWAYS ${Boolean(value)}`.padStart(30))
// }

// // Numbers and Strings
// console.log(1 + "2") // String
// console.log("1" + 2) // String
// console.log("" + 2 - 3) // Number
// console.log("7" * "2") // Number
// console.log(4 + 10 + "px") // Firstly (4+10)(numbers) then string '14px'
// console.log("px" + 4 + 10) // String because "px"+4 is String and "px4"+10 is String
// console.log("42" - 30) // Number
// console.log("42px" - 30) // NaN
// console.log(null + 2) // Number
// console.log(undefined + 2) // NaN

// == vs ===
