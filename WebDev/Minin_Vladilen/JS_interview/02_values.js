// let a = 77
// let b = a
// b++
// console.log(a, b)

const a = [1, 2, 3]
// const b = a.concat()
const b = a
b[1]++
console.log(a, b)
const c = [1, 3, 3]
console.log(a === b)
console.log(a === c)

console.log()
console.log([] === [])
console.log([] == [])
console.log({} === {})
console.log({} == {})
