class Animal {
  static type = "ANIMAL";

  constructor(options) {
    this.name = options.name;
    this.age = options.age;
    this.hasTail = options.hasTail;
  }

  voice() {
    console.log("I'm an Animal.");
  }
}

class Cat extends Animal {
  static type = "CAT";

  constructor(options) {
    super(options);
    this.color = options.color;
  }

  voice() {
    super.voice();
    console.log("I'm a Cat.");
  }
}

const animal = new Animal({
  name: "Animal",
  age: 7,
  hasTail: true
});

const cat = new Cat({
  name: "Animal",
  age: 7,
  hasTail: true,
  color: "black"
})