function* myGen(n = 10) {
  for (let i = 0; i < n; i++) {
    yield i;
  }
}

for (let i of myGen(6)) {
  console.log(i);
}

const gen = myGen(6);
console.log(gen.next());
console.log(gen.next());
console.log(gen.next());
console.log(gen.next());
console.log(gen.next());
console.log(gen.next());
console.log(gen.next());