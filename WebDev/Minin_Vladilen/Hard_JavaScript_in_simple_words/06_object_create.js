const person = Object.create(
  {
    sayHello() {
      console.log("Hello!");
    }
  },
  {
    name: {
      value: "John",
      enumerable: true,
      writable: true,
      configurable: true
    },
    age: {
      value: 20,
      enumerable: false,
      writable: false,
      configurable: false,
    },
    job: {
      get() {
        return this.name;
      }
    }
  }
);

person.name = "Maxim";
console.log(person);
console.log(Object.keys(person));
