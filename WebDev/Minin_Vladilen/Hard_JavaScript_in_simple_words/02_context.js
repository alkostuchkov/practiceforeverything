function hello() {
  console.log("Hello", this);
}

const robot = {
  name: "Verter",
  sn: 12345,
  hello: hello,
  // helloGlobal: hello.bind(window),
  // helloGlobal: hello.bind(this)
  showInfo(profile, isOldModel) {
    console.group(`${this.name} info:`);
    console.log(`Name: ${this.name}.`);
    console.log(`SN: ${this.sn}.`);
    console.log(`Profile: ${profile}.`);
    console.log(`Old model: ${isOldModel}.`);
    console.groupEnd();
  }
};

const smallRobot = {
  name: "B2",
  sn: 56780
};

// robot.hello();
// robot.helloGlobal();
// robot.showInfo("Historic", false);
// robot.showInfo.bind(smallRobot, "Mechanic", true)();
// robot.showInfo.apply(smallRobot, ["Mechanic", true]);
robot.showInfo.call(smallRobot, "Mechanic", true);

const arr = [1, 2, 3, 4, 5];
Array.prototype.multipleBy = function (n) {
  return this.map(item => item * n);
};
console.log(arr.multipleBy(2));
