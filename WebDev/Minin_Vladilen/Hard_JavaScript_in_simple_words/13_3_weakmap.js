const people = [{name: "Elena"}, {name: "John"}];
const wm = new WeakMap();
// wm.set(people[0], "Elena");
// wm.set(people[1], "John");
people.forEach(person => wm.set(person, new Date()));
console.log(wm.has(people[0]));
console.log(wm.get(people[1]));
people[1] = null;
console.log(wm.get(people[1]));
