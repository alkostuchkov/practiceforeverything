const person = {
  name: "John",
  age: 20
};

const keys = Object.keys(person);
const values = Object.values(person);
const entries = Object.entries(person);
console.log(keys);
console.log(values);
console.log(entries);
console.log(Object.fromEntries(entries));

const m = new Map();
const names = ["Maxim", "Olga", "Ann", "Victor", "John"];
const phones = [
  "+111-22-333333",
  "+111-33-444444",
  "+111-44-555555",
  "+111-55-777777",
  "+111-66-888888"
];

for (let i = 0; i < names.length; i++) {
  m.set(names[i], phones[i]);
}

// console.log(m, m.size);
// m.set("Maxim", "NewPhone");
// console.log(m, m.size);
// console.log(m.get("Alex"));

// // for (let [key, value] of m.entries()) {
// for (let [key, value] of m) {
//   console.log(key, "->", value);
// }

const newMap = new Map(Object.entries(person));
console.log(newMap);
