// function calcFunction(n) {
//   return function () {
//     console.log(n * 1000);
//   }
// }

// const cF = calcFunction(5);
// cF();

// function incrementNumber(increment) {
//   return function (number) {
//     return number += increment;
//   }
// }
// const addOne = incrementNumber(1);
// console.log(addOne(10));

// function domainGenerator(domain) {
//   return function (url) {
//     return `https://www.${url}.${domain}`;
//   }
// }
// const comDomain = domainGenerator("com");
// console.log(comDomain("google"));

// function logPerson() {
//   console.log(`Person: ${this.name}, ${this.age}, ${this.job}`);
// }

const person1 = {name: "Mark", age: 20, job: "Fronend"};
const person2 = {name: "Elena", age: 18, job: "Backend"};

// logPerson.bind(person1)();
// logPerson.bind(person2)();

// function myBind(context, fn) {
//     return function (...args) {
//     return fn.apply(context, args);
//   };
// }
// myBind(person2, logPerson)();
// myBind(person1, logPerson)();

// function logPerson(phone, email) {
function logPerson() {
  console.log(`Person: ${this.name}, ${this.age}, ${this.job}`);
  // console.log(
  // `Person: ${this.name}, ${this.age}, ${this.job}, ${phone}, ${email}`
  // );
}

function newBind(fn, context, ...args) {
  return function (...pars) {
    const uniqId = Date.now().toString();
    context[uniqId] = fn;
    const result = context[uniqId](...args.concat(pars));
    delete context[uniqId];
    return result;
  };
}
newBind(logPerson, person1)("111-22-22-22", "u@h.com");
newBind(logPerson, person1, "111-22-22-22")("u@h.com");
newBind(logPerson, person1, "111-22-22-22", "u@h.com")();
console.log(person1);
