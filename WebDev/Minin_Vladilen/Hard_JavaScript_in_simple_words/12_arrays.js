const people = [
  {name: "John", age: 13, budget: 1700},
  {name: "Maxim", age: 11, budget: 1000},
  {name: "Alexander", age: 45, budget: 1400},
  {name: "Elena", age: 25, budget: 1300},
  {name: "Victoria", age: 27, budget: 1350},
  {name: "Victor", age: 38, budget: 1500}
];

// // For of
// for (let person of people) {
//   console.log(person);
// }

// // ForEach
// people.forEach(person => console.log(person));

// // Map
// const newPeople = people.map(person => person.budget + person.budget * 0.15);
// console.log(newPeople);

// // Filter
// const adults = people.filter(person => {
//   if (person.age >= 18) {
//     return true;
//   }
// });
// const adults = people.filter(person => person.age >= 18);
// console.log(adults);

// // Reduce
// const amount = people.reduce((total, person) => total + person.budget, 0);
// console.log(amount);

// // Find
// const john = people.find(person => person.name === "John");
// console.log(john);

// FindIndex
const john_index = people.findIndex(person => person.name === "John");
console.log(john_index);
