// const wringOut = times =>
//   new Promise((resolve, reject) => {
//     if (times > 100) {
//       reject(`Too many times. Can't do ${times} wringouts.`);
//     } else {
//       resolve(times);
//     }
//   });

// const pushUp = times =>
//   new Promise((resolve, reject) => {
//     if (times > 50) {
//       reject(`Too many times. Can't do ${times} pushups.`);
//     } else {
//       resolve(times);
//     }
//   });

// //Promises
// wringOut(20)
//   .then(times => {
//     console.log(`I've done ${times} wringouts.`);
//     return pushUp(55);
//   })
//   .then(times => {
//     console.log(`I've done ${times} pushups.`);
//   })
//   .catch(err => console.log("Error:", err))
//   .finally(() => console.log("Finally"));

// //async await
// const doWorkOut = async () => {
//   try {
//     await wringOut(20);
//     // console.log(`I've done ${times} wringouts.`);
//     console.log(`I've done wringouts.`);
//     await pushUp(55);
//     console.log(`I've done pushups.`);
//     // console.log(`I've done ${times} pushups.`);
//   } catch (err) {
//     console.log("Error:", err);
//   } finally {
//     console.log("Finally");
//   }
// };

// doWorkOut();

// //Vladilen Minin
// const p = new Promise((resolve, reject) => {
//   console.log("Prepearing data...");
//   const backendData = {
//     modified: false,
//     someAttr: "SomeAttr"
//   };
//   let isSuccess = true;
//   if (isSuccess) {
//     resolve(backendData);
//   } else {
//     reject("Error");
//   }
// });

// p.then(data => {
//   return new Promise((resolve, reject) => {
//     console.log("Received data", data);
//     data.modified = true;
//     let isSuccess = true;
//     if (isSuccess) {
//       resolve(data);
//     } else {
//       reject("Error");
//     }
//   });
// })
//   .then(clientData => {
//     console.log("Received and modified data", clientData);
//     return clientData;
//   })
//   .then(data => {
//     data.newAttr = "newAttr";
//     console.log("One more modified data", data);
//   })
//   .catch(err => console.log(err))
//   .finally(() => console.log("Finally"));

//My practice
const promise = new Promise((resolve, reject) => {
  const backendData = {
    modified: false
  }
  console.log("backendData", backendData)
  let isSuccess = true
  if (isSuccess) {
    resolve(backendData)
  } else {
    reject("Error 1")
  }
})
  .then(data => {
    data.newAttr = "newAttr"
    data.modified = true
    console.log("Modified data", data)
    return data
  })
  .then(clientData => {
    clientData.oneMoreAttr = "oneMoreAttr"
    console.log("clientData", clientData)
  })
  .catch(err => console.log("Error 2:", err))
  .finally(console.log("Finally"))

// const sleep = ms =>
//   new Promise(resolve => {
//     setTimeout(() => resolve(), ms);
//   });

// // sleep(3000).then(() => console.log("After 3 sec"));
// Promise.all([sleep(2000), sleep(5000)]).then(() => console.log("All promises"));
// Promise.race([sleep(2000), sleep(5000)]).then(() =>
//   console.log("Race promises")
// );
