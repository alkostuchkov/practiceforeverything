import React, {useState} from "react"

function App() {
  const [text1, setText1] = useState("Hello")
  const [text2, setText2] = useState("Hi")

  function showText1() {
    setText1("Text1")
  }

  function showText2(event) {
    // console.log(event.target.value)
    setText2(event.target.value)
  }

  function mouseMoved(event) {
    console.log(event.target.textContent)
  }

  return (
    <div className="App">
      <button onClick={showText1}>Push</button>
      {/* <input defaultValue={text1} onInput={showText2} /> */}
      <input onInput={showText2} />
      <p>{text1}</p>
      <p>{text2}</p>
      <div onMouseMove={mouseMoved}>Lorem</div>
    </div>
  )
}

export default App
