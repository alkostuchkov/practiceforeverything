import React from "react";
import classes from "./Header.module.css";

function Header() {
  return (
    <div className={classes.header}>
      <h1>React App</h1>
      <h2>slogan</h2>
    </div>
  );
}

export default Header;
