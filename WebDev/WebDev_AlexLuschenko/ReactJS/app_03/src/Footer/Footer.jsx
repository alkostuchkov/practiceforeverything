import React from "react";
import classes from "./Footer.module.css";

function Footer() {
  return (
    <div className={classes.footer}>
      <b>React site</b>
      <p>Copyright 2021. All rights reserved.</p>
    </div>
  );
}

export default Footer;
