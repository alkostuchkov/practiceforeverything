import React from "react";
import classes from "./App.module.css";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import Main from "../Main/Main";
import Sidebar from "../Sidebar/Sidebar";
import articles from "../data/articles.json";

function App() {
  return (
    <div className={classes.container}>
      <Header />
      <Main articles={articles} />
      <Sidebar articles={articles} />
      <Footer />
    </div>
  );
}

export default App;
