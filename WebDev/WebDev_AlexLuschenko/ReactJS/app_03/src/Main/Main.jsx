import React from "react";
import classes from "./Main.module.css";
import ArticleTitle from "../ArticleTitle/ArticleTitle";
import ArticleBody from "../ArticleBody/ArticleBody";

function Main({articles}) {
  return (
    <div className={classes.main}>
      {articles.map((article, index) => (
        <section key={index}>
          <ArticleTitle title={article.title} />
          <ArticleBody body={article.body} />
          {/* <h2>{article.title}</h2> */}
          {/* <div>{article.body}</div> */}
        </section>
      ))}
    </div>
  );
}

export default Main;
