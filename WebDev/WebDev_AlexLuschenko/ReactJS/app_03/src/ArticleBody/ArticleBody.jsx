import React from "react";
import classes from "./ArticleBody.module.css";

function ArticleBody(props) {
  return <div className={classes.article_body}>{props.body}</div>;
}

export default ArticleBody;
