import React from "react";
import classes from "./Sidebar.module.css";

function Sidebar({articles}) {
  return (
    <div className={classes.sidebar}>
      <nav>
        <ul>
          {articles.map((article, index) => (
            <li key={index}>
              <a href={article.href} target="_blank" rel="noreferrer">
                {article.title}
              </a>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  );
}

export default Sidebar;
