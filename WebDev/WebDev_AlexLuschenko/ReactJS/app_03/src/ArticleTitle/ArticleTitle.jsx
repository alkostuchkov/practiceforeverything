import React from "react";
import classes from "./ArticleTitle.module.css";

function ArticleTitle(props) {
  return (
    <div className={classes.article_title}>
      <h2>{props.title}</h2>
    </div>
  );
}

export default ArticleTitle;
