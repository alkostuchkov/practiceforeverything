let primer = document.querySelector(".primer")
console.log(primer)
console.log(primer.innerHTML)
console.log(primer.innerText)
console.log(primer.textContent)
// document.querySelector("#out-1").innerHTML = primer.innerHTML  //if you need HTML
// document.querySelector("#out-1").innerText = primer.innerText  //if you need a text (Internet Explorer)
document.querySelector("#out-1").textContent = primer.textContent //if you need a text (gets content of all elements)
