// input type="text"
document.querySelector(".btn-1").addEventListener("click", () => {
  let data = document.querySelector(".in-1").value;
  document.querySelector(".out-1").textContent = data;
});

// input type="number"
document.querySelector(".btn-2").addEventListener("click", () => {
  let data = document.querySelector(".in-2").value;
  document.querySelector(".out-2").textContent = data;
});

// input type="color"
document.querySelector(".btn-3").addEventListener("click", () => {
  let data = document.querySelector(".in-3").value;
  document.querySelector(".out-3").textContent = data;
});

// input type="date"
document.querySelector(".btn-4").addEventListener("click", () => {
  let data = document.querySelector(".in-4").value;
  document.querySelector(".out-4").textContent = data;
});

// input type="range"
// document.querySelector(".btn-5").addEventListener("click", e => {
// document.querySelector(".out-5").textContent = document
// .querySelector(".in-5").value;
document.querySelector(".in-5").addEventListener("input", () => {
  let data = document.querySelector(".in-5").value;
  document.querySelector(".out-5").textContent = data;
});

// input type="checkbox"
document.querySelector(".in-6").addEventListener("change", () => {
  let data = document.querySelector(".in-6").checked;
  document.querySelector(".out-6").textContent = data;
});

document.querySelector(".btn-6").addEventListener("click", () => {
  if (document.querySelector(".in-6").checked) {
    document.querySelector(".in-6").checked =
      !document.querySelector(".in-6").checked;
  } else {
    document.querySelector(".in-6").checked =
      !document.querySelector(".in-6").checked;
  }
});

// input type="radio"
document.querySelector(".btn-7").addEventListener("click", () => {
  let radios = document.querySelectorAll(".in-7");
  radios.forEach(radio => {
    if (radio.checked) {
      let data = radio.value;
      document.querySelector(".out-7").textContent = data;
    }
  });
});

// input type="password"
document.querySelector(".btn-8").addEventListener("click", () => {
  let data = document.querySelector(".in-8").value;
  document.querySelector(".out-8").textContent = data;
  // document.querySelector(".in-8").type = "text";
});

// input type="email"
document.querySelector(".btn-9").addEventListener("click", () => {
  let data = document.querySelector(".in-9").value;
  document.querySelector(".out-9").textContent = data;
});

// select
document.querySelector(".btn-10").addEventListener("click", () => {
  document.querySelector(".in-10").value = "value 2";
  let data = document.querySelector(".in-10").value;
  document.querySelector(".out-10").textContent = data;
});
// document.querySelector(".in-10").addEventListener("change", function () {
//   // let data = document.querySelector(".in-10").value;
//   // document.querySelector(".out-10").textContent = data;
//   document.querySelector(".out-10").textContent = this.value;
// });

// textarea
document.querySelector(".in-11").textContent = "Hello,\nWorld!";
document.querySelector(".btn-11").addEventListener("click", () => {
  let data = document.querySelector(".in-11").value;
  document.querySelector(".out-11").textContent = data;
});

// form
document.querySelector(".form-main").addEventListener("submit", e => {
  e.preventDefault();
  const formMain = document.querySelector(".form-main");
  console.dir(formMain);
  console.dir(formMain.elements.rb_12.value);
  console.dir(formMain.elements.my_select.value);
});
