const outNumber = document.querySelector(".out-number");
const outSeconds = document.querySelector(".out-seconds");
const btn = document.querySelector(".btn");
let start = 0;
const increment = 5;
const number = 1000;
const time = 10; //seconds to reach the number
const totalMS = (time / (number / increment)) * 1000; // ms to do 1 increment of the increment times
// number / increment = (times of increments to reach the number)
let counter = 0;

btn.addEventListener("click", () => {
  const idIntervalSeconds = setInterval(() => {
    outSeconds.textContent = ++counter + " seconds";
  }, 1000); //to count remain seconds

  const idInterval = setInterval(() => {
    start += increment;
    outNumber.textContent = start;
    if (start >= number) {
      clearInterval(idInterval);
      clearInterval(idIntervalSeconds);
    }
  }, totalMS);
});
