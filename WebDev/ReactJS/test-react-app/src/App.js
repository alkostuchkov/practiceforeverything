// import "./App.css";
import React from "react";

function App() {
  const a = 4;
  const arr = [1, 2, 3];
  const st = {background: "orange"};
  const cl = "yellow";
  const person = {
    firstName: "John",
    age: 14
  };
  const personKeys = Object.keys(person);

  return (
    <div className="App">
      <h1>JSX output</h1>
      <h2>Основные конструкции JSX</h2>
      <p>Variable's output: {a}</p>
      <p>Curl brackets' output: {"{}"}</p>
      <p>Array's output: {`[${arr.join(", ")}]`}</p>
      <p>
        {arr.map((item, index) => (
          <strong key={index}>{item}</strong>
        ))}
      </p>
      <p style={st}>Inline styles</p>
      <p style={{backgroundColor: "black", color: cl}}>Inline style 2</p>
      <p>Object's output: {JSON.stringify(person)}</p>
      <p>{Object.keys(person).map(key => `${key} -> ${person[key]} `)}</p>
      <ul>
        {personKeys.map(key => (
          <li key={key}>
            {/* {key} {"->"} {person[key]} */}
            {`${key} -> ${person[key]}`}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default App;
