// function factorial(n) {
//   if (n === 1) {
//     return n
//   } else {
//     return n * factorial(n - 1)
//   }
// }

// console.log(factorial(5))

// const factorial = n => (n === 1 ? n : n * factorial(n - 1))
// console.log(factorial(5))

// let counter = 0

// function recurse() {
//   if (counter === 5) return console.log("Done")

//   counter++
//   return recurse()
// }
// recurse()

// function myPow(x, y) {
//   if (y === 1) return x
//   // if (y === 0) return 1

//   return x * myPow(x, --y)
// }
// console.log(myPow(2, 3))

function amountOfDigits(number) {
  console.log(number / 10)
  // if (!number) return 0
  if (number / 10 < 1) return 1

  return 1 + amountOfDigits(number / 10)
}
console.log(amountOfDigits(10245))
