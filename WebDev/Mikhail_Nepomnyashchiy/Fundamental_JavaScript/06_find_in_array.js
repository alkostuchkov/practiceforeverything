const numbers = [1, 2, 3, 4, 5]
const langs = ["Python", "JavaScript", "Ruby", "C++"]
const people = [
  {id: 1, firstName: "John", age: 13},
  {id: 2, firstName: "Helen", age: 23},
  {id: 3, firstName: "Maxim", age: 11},
  {id: 4, firstName: "Michael", age: 43}
]

let result = numbers.indexOf(4)
console.log("Result:", result)
result = langs.indexOf("Ruby")
console.log("Result:", result)

result = langs.includes("Ruby")
console.log("Result:", result)

result = people.find(person => person.id === 3)
console.log("Result:", result)

result = people.findIndex(person => person.id === 3)
console.log("Result:", result)
