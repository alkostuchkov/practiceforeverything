const numbers = [1, 2, 3, 4, 5]
const langs = ["Python", "JavaScript", "Ruby", "C++"]
const people = [
  {id: 1, firstName: "John", age: 13},
  {id: 2, firstName: "Helen", age: 23},
  {id: 3, firstName: "Maxim", age: 11},
  {id: 4, firstName: "Michael", age: 43}
]

let result = numbers.forEach(item => item ** 2)
console.log("Result:", result)

result = numbers.map(item => item ** 2)
console.log("Result:", result)

result = people.filter(person => person.age < 20)
console.log("Result:", result)

result = people
  .filter(person => person.age < 20)
  .reduce((accum, person) => accum + person.age, 0)
console.log("Result:", result)
