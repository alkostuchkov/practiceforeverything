function createFn(name) {
  const msg = `Hello, ${name}!`

  function greeting() {
    console.log(msg)
  }

  return greeting
}

const sayHello = createFn("John")
sayHello()
