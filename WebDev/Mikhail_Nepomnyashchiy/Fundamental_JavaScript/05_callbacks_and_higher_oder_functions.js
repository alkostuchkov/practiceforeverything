function numSquare(num) {
  return num ** 2
}

function numCube(num) {
  return num ** 3
}

function modifyArrayNums(arr, modifyFunction) {
  const outputArray = []

  for (let i = 0; i < arr.length; i++) {
    outputArray.push(modifyFunction(arr[i]))
  }
  return outputArray
}

let newArr = modifyArrayNums([1, 2, 3, 4, 5], numSquare)
console.log(newArr)
newArr = modifyArrayNums([1, 2, 3, 4, 5], numCube)
console.log(newArr)
