// // Global object
// globalThis.console.log("Hello")

// // Modules
// var sayHello = function () {}
// // globalThis.sayHello()
// console.log(module)

// // Creating a Module
// const logger = require("./logger")

// logger.log("From main app")

// console.log(logger)

// // Module Wrapper Function
// const logger = require("./logger");
// logger.log("From main app");

// // Node.js's API
// // Path Module
// const path = require("path");
// const pathObj = path.parse(__filename);
// console.log(pathObj);

// // OS Module
// const os = require("os");
// console.log(os.userInfo());
// console.log(os.type());
// console.log(os.totalmem() / 1024 ** 3);

// // File System Module
// const fs = require("fs");
// const files = fs.readdirSync("./");
// console.log(files);

// fs.readdir("./", (err, files) => {
//   if (err) console.log(err);
//   else console.log(files);
// });

// // Events Module and Event Arguments
// const EventEmitter = require("events");

// const emitter = new EventEmitter();
// // Register a Listener
// emitter.on("myEvent", eventArg => console.log("myEvent occured.", eventArg));
// // on adn addListener are the same!!!
// // emmiter.addListener("myEvent", () => console.log("myEvent occured."));
// // Raise a myEvent
// emitter.emit("myEvent", {id: 1, url: "https://"});

// // Extending EventEmitter
// class MyEventEmitter extends EventEmitter {}

// const myEventEmitter = new MyEventEmitter();
// myEventEmitter.on("myEvent", () => console.log("MyEvent occured."));
// myEventEmitter.emit("myEvent");

// // Exercise
// const Logger = require("./logger");
// const logger = new Logger();
// logger.on("logging", eventArg => console.log("logging", eventArg));

// logger.log("From main app");

// HTTP Module
const http = require("http");
const server = http.createServer((req, res) => {
  if (req.url === "/") {
    res.write("Hello, World!");
    res.end();
  }

  if (req.url === "/api/courses") {
    res.write(
      JSON.stringify([
        {id: 1, courseName: "JavaScript"},
        {id: 2, courseName: "Python"}
      ])
    );
    res.end();
  }
});

// server.on("connection", socket => console.log("New connection..."));

server.listen(3000);

console.log("Listening on port 3000...");
