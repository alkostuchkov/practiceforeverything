const express = require("express");
const app = express();
const port = 3000;

const users = [
  {id: 1, userName: "John"},
  {id: 2, userName: "Max"},
  {id: 3, userName: "Alexander"}
];

app.get("/", (req, res) => {
  // console.log(req);
  res.send("Hello, World!");
});

app.get("/api/users", (req, res) => {
  res.send(users);
  // res.send([1, 2, 3]);
});

app.listen(port, () => console.log("Listening on port 3000..."));
