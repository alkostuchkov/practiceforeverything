// console.log("Before");
// const user = getUser(1);
// console.log(user);
// console.log("After");

// function getUser(id, callback) {
//   setTimeout(() => {
//     console.log("Reading a user from a database...");

//     return {id: id, gitHubUserName: "john"};
//   }, 2000);
// }

// // Solution 1: Callbacks
// console.log("Before");
// getUser(1, user => {
//   getRepositories(user.gitHubUsername, repositories => {
//     console.log(`${user.gitHubUsername}'s repositories: ${repositories}`);
//   });
// });
// console.log("After");

// function getUser(id, callback) {
//   setTimeout(() => {
//     console.log("Reading a user from a database...");
//     callback({id: id, gitHubUsername: "john"});
//   }, 2000);
// }

// function getRepositories(username, callback) {
//   setTimeout(() => {
//     console.log(`Getting user repositories...`);
//     callback(["repo1", "repo2", "repo3"]);
//   }, 2000);
// }

// // Solution 1.1: Callbacks + Named Functions
// console.log("Before");
// getUser(1, displayUser);
// console.log("After");

// function displayUser(user) {
//   getRepositories(user.gitHubUsername, displayRepositories);
// }

// function displayRepositories(repositories) {
//   console.log(`${user.gitHubUsername}'s repositories: ${repositories}`);
// }

// function getUser(id, callback) {
//   setTimeout(() => {
//     console.log("Reading a user from a database...");
//     callback({id: id, gitHubUsername: "john"});
//   }, 2000);
// }

// function getRepositories(username, callback) {
//   setTimeout(() => {
//     console.log(`Getting user repositories...`);
//     callback(["repo1", "repo2", "repo3"]);
//   }, 2000);
// }

// // Solution 2: Promises
// console.log("Before");

// getUser(1)
//   .then(user =>
//     getRepositories(user.gitHubUsername)
//       .then(repositories => console.log(repositories))
//       .catch(error => console.log("Error:", error.message))
//   )
//   .catch(error => console.log("Error:", error.message));

// // Better than above!!!
// getUser(1)
//   .then(user => getRepositories(user.gitHubUsername))
//   .then(repositories => console.log(repositories))
//   .catch(error => console.log("Error:", error.message));

// console.log("After");

// function getUser(id) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       console.log("Reading a user from a database...");
//       resolve({id: id, gitHubUsername: "john"});
//       // reject(new Error(`can't get the user with id = 1.`));
//     }, 2000);
//   });
// }

// function getRepositories(username) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       console.log(`Getting ${username}'s repositories...`);
//       resolve(["repo1", "repo2", "repo3"]);
//       // reject(new Error(`can't get ${username}'s repositories.`));
//     }, 2000);
//   });
// }

// Solution 3: Async/Await
console.log("Before");

async function displayRepositories() {
  try {
    const user = await getUser(1);
    const repositories = await getRepositories(user.gitHubUsername);
    console.log(repositories);
  } catch (error) {
    console.log("Error:", error.message);
  }
}
displayRepositories();

console.log("After");

function getUser(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Reading a user from a database...");
      resolve({id: id, gitHubUsername: "john"});
      // reject(new Error(`can't get the user with id = 1.`));
    }, 2000);
  });
}

function getRepositories(username) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(`Getting ${username}'s repositories...`);
      resolve(["repo1", "repo2", "repo3"]);
      // reject(new Error(`can't get ${username}'s repositories.`));
    }, 2000);
  });
}
