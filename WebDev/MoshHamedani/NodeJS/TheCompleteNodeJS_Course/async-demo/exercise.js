// getCustomer(1)
//   .then(customer => {
//     console.log("Customer: ", customer);
//     if (customer.isGold) {
//       getTopMovies()
//         .then(topMovies => {
//           console.log("Top movies: ", topMovies);
//           sendEmail(customer.email, topMovies)
//             .then(() => console.log("Email sent..."))
//             .catch(error => console.log("Error: ", error.message));
//         })
//         .catch(error => console.log("Error: ", error.message));
//     }
//   })
//   .catch(error => console.log("Error: ", error.message));

async function notifyCustomer() {
  try {
    const customer = await getCustomer(1);
    console.log("Customer: ", customer);
    if (customer.isGold) {
      const topMovies = await getTopMovies();
      console.log("Top movies: ", topMovies);
      await sendEmail(customer.email, topMovies);
      console.log("Email sent...");
    }
  } catch (error) {
    console.log("Error: ", error.message);
  }
}
notifyCustomer();

function getCustomer(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        id: 1,
        name: "Mosh Hamedani",
        isGold: true,
        email: "email"
      });
      // reject(new Error("Can't get the customer."));
    }, 4000);
  });
}

function getTopMovies() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(["movie1", "movie2"]);
      // reject(new Error("Can't get movies"));
    }, 4000);
  });
}

function sendEmail(email, movies) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
      // reject(new Error("Can't send an email."));
    }, 4000);
  });
}

// getCustomer(1, (customer) => {
//   console.log('Customer: ', customer);
//   if (customer.isGold) {
//     getTopMovies((movies) => {
//       console.log('Top movies: ', movies);
//       sendEmail(customer.email, movies, () => {
//         console.log('Email sent...')
//       });
//     });
//   }
// });

// function getCustomer(id, callback) {
//   setTimeout(() => {
//     callback({
//       id: 1,
//       name: 'Mosh Hamedani',
//       isGold: true,
//       email: 'email'
//     });
//   }, 4000);
// }

// function getTopMovies(callback) {
//   setTimeout(() => {
//     callback(['movie1', 'movie2']);
//   }, 4000);
// }

// function sendEmail(email, movies, callback) {
//   setTimeout(() => {
//     callback();
//   }, 4000);
// }
