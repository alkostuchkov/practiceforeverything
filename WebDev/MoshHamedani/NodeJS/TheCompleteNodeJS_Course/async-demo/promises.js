const p = new Promise((resolve, reject) => {
  setTimeout(() => {
    // resolve(1); // pendig => resolved, fulfilled
    reject(new Error("'Error message'")); //pending => rejected
  }, 2000);
});

p
  .then(result => console.log("Result:", result))
  .catch(error => console.log("Error:", error.message));
