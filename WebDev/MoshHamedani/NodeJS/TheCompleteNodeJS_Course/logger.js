// // (function (exports, require, module, __filename, __dirname) {
// console.log(__filename);
// console.log(__dirname);

const EventEmitter = require("events");

const url = "https://mylogger.io/log";

class Logger extends EventEmitter {
  log(message) {
    // Send an HTTP request
    console.log("In logger.js");
    console.log(message);
    // Raise a "logging" event.
    this.emit("logging", {data: message, url: url});
  }
}

module.exports = Logger;
// module.exports = log
// module.exports.url = url
// module.exports.myUrl = url

// console.log(module);

// });
