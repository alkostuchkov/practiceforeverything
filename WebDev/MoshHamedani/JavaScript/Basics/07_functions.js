// // Function Declaration
// function walk(...args) {
//   console.log("walk")
//   console.log(args)
//   // console.log(arguments)
//   // for (let arg of arguments) console.log(arg)
// }

// walk(1, 2, 77)
// // Function Expression
// const run = function () {
//   console.log("run")
// }

// // Getters and Setters
// const person = {
//   firstName: "firstName",
//   lastName: "lastName",
//   get fullName() {
//     return `${person.firstName} ${person.lastName}`
//   },
//   set fullName(fullName) {
//     if (typeof fullName !== "string") {
//       throw new Error("fullName must be a string!")
//     }
//     const parts = fullName.split(" ")
//     if (parts.length !== 2) {
//       throw new Error(
//         "fullName must have firstName and lastName separated by space!"
//       )
//     }
//     this.firstName = parts[0]
//     this.lastName = parts[1]
//   }
// }

// // console.log(person.fullName)
// // person.fullName = "John Anonymous"
// // console.log(person.fullName)
// // console.log(person)

// // Try and Catch
// try {
//   person.fullName = "John"
// } catch (e) {
//   console.log(e)
// }

// console.log("After try catch")

// // This keyword
// const video = {
//   title: "a",
//   tags: ["a", "b", "c"],
//   showTags() {
//     this.tags.forEach(function (tag) {
//       console.log(this.title, tag)
//     }, this)
//     // }, video)
//   }
// }
// video.showTags()

// // Changing this
// const video = {
//   title: "a",
//   tags: ["a", "b", "c"],
//   showTags() {
//     const self = this
//     this.tags.forEach(function (tag) {
//       console.log(self.title, tag)
//     })
//   }
// }

// // Solution 1
// const video = {
//   title: "a",
//   tags: ["a", "b", "c"],
//   showTags() {
//     const self = this
//     this.tags.forEach(function (tag) {
//       console.log(self.title, tag)
//     })
//   }
// }
// video.showTags()

// // Solution 2
// function showVideo(a, b) {
//   console.log(this)
//   console.log(a, b)
// }
// showVideo.call(video, 1, 2)
// showVideo.apply(video, [1, 2])
// showVideo.bind(video, [1, 2])()

// const video = {
//   title: "a",
//   tags: ["a", "b", "c"],
//   showTags() {
//     this.tags.forEach(
//       function (tag) {
//         console.log(this.title, tag)
//       }.bind(this)
//     )
//   }
// }
// video.showTags()

// // Solution 3
// const video = {
//   title: "a",
//   tags: ["a", "b", "c"],
//   showTags() {
//     this.tags.forEach(tag => console.log(this.title, tag))
//     // arrow functions inherit this from the containing function (showTags) !!!
//   }
// }
// video.showTags()

// // Excercises
// // 1
// function summa(...items) {
//   if (items.length === 1 && Array.isArray(items[0]))
//     items = items[0]

//   return items.reduce((total, item) => total + item)
// }
// const arr = [1, 2, 3, 4]
// console.log(summa(1, 2, 3, 4))
// console.log(summa(arr))

// // 2
// const circle = {
//   radius: 1,
//   get getRadius() {
//     return this.radius
//   },
//   set setRadius(radius) {
//     this.radius = radius
//   },
//   get area() {
//     return Math.PI * this.radius ** 2
//   }
// }
// console.log(circle.getRadius)
// console.log(circle.area)
// circle.setRadius = 2
// console.log(circle.getRadius)

// 3
function countOccurrences(array, serachElement) {
  if (!Array.isArray(array)) 
    throw new Error("Invalid array.")

  return array.reduce((accumulator, item) => {
    const occurrences = item === serachElement ? 1 : 0

    return accumulator + occurrences
  }, 0)
}
try {
  const arr = [1, 2, 3, 4, 1, 3, 3]
  console.log(countOccurrences(arr, 1))
  console.log(countOccurrences(true, 3))
} catch (e) {
  console.log("In catch::", e.message)
}
