// // Factory function
// const createCircle = function (radius) {
//   return {
//     radius,
//     draw() {
//       console.log("draw")
//     }
//   }
// }
// const circle = createCircle(25)
// circle.draw()

// // Construcor
// function Circle(radius) {
//   this.radius = radius
//   this.draw = function () {
//     console.log("draw")
//   }
// }
// const circle2 = new Circle(30)
// circle2.draw()

// const Circle2_1 = new Function(
//   "radius",
//   `this.radius = radius
//   this.draw = function () {
//     console.log("draw")
//   }`
// )
// const circle2_1 = new Circle2_1(1)

// class CircleClass {
//   constructor(radius) {
//     this.radius = radius
//   }

//   draw() {
//     console.log("draw")
//   }
// }
// const circle3 = new CircleClass(77)
// circle3.draw()

// const circle = {
//   radius: 1,
//   draw() {
//     console.log("draw")
//   }
// }
// for (let key in circle) {
//   if (circle.hasOwnProperty) console.log(key, circle[key])
// }
// for (let key of Object.keys(circle)) {
//   console.log(key, circle[key])
// }
// console.log(Object.entries(circle))
// for (let [key, value] of Object.entries(circle)) {
//   console.log(key, "->", value)
// }
// if ("radius" in circle) console.log("yes")
// if ("toString" in circle) console.log("yes")

// // Cloning objects
// const circle = {
//   radius: 1,
//   draw() {
//     console.log("draw")
//   }
// }
// const another = {}
// // 1
// for (let key of Object.keys(circle)) {
//   another[key] = circle[key]
// }
// console.log(another)
// another.radius = 25
// console.log(circle.radius, another.radius)
// // 2
// const another = Object.assign(
//   // {color: "cyan"},
//   circle,
//   {name: "John"},
//   {age: 14}
// )
// console.log(another)

// // 3
// const another = {...circle}
// console.log(another)
// another.radius = 25
// console.log(circle.radius, another.radius)

// // Strings `` doesn't need \n
// const name = "John"
// const message = `Hi, ${name}!
// This is my message for you.
// Bye!!!`
// console.log(message)

// // Date
// const now = new Date()
// console.log(now)
// console.log(now.toDateString())
// console.log(now.toTimeString())
// console.log(now.toISOString()) // common used time format in web apps between client and server
// const date1 = new Date("April 07 1976 09:00")
// console.log(date1)
// console.log(date1.toDateString())
// const date2 = new Date(1976, 03, 07, 09, 00, 10, 00)
// console.log(date2)
// console.log(date2.toDateString())
// console.log(now.getFullYear())

// Exercises
// // 1
// const address = {
//   street: "Street",
//   city: "City",
//   zipCode: 1234567
// }

// function showAddress(address) {
//   for (let key of Object.keys(address)) {
//     console.log(`${key} -> ${address[key]}`)
//   }
// }

// showAddress(address)

// // 2
// // Factory function
// function createAddress(street, city, zipCode) {
//   return {
//     street,
//     city,
//     zipCode
//   }
// }

// // Constructor function
// function Address(street, city, zipCode) {
//   this.street = street,
//   this.city = city,
//   this.zipCode = zipCode
// }

// const address1 = new Address("Street", "City", 1234567)
// const address2 = new Address("Street", "City", 1234567)
// const a = {
//   street: "Street"
// }

// function areEqual(address1, address2) {
//   for (let key of Object.keys(address1)) {
//     if (address1[key] !== address2[key]) return false
//   }
//   return true
// }

// function areSame(address1, address2) {
//   return address1 === address2
// }

// console.log(areEqual(address1, address2))
// console.log(areSame(address1, address2))
// console.log(areEqual(address1, a))

// // 3
// const blogPost = {
//   title: "Title",
//   body: "Body",
//   author: "Author",
//   views: 12,
//   comments: [
//     {
//       author: "Comment Author",
//       body: "Comment1"
//     },
//     {
//       author: "Comment Author",
//       body: "Comment2"
//     }
//   ],
//   isLive: true
// }
// console.log(blogPost)

// // 4
// function BlogPost(title, body, author) {
//   this.title = title,
//   this.body = body,
//   this.author = author,
//   this.views = 0,
//   this.comments = [],
//   this.isLive = false
// }

// // 5
// const priceRanges = [
//   {label: "$", tooltip: "Inexpencive", minPerPerson: 0, maxPerPerson: 10},
//   {label: "$$", tooltip: "Moderate", minPerPerson: 11, maxPerPerson: 20},
//   {label: "$$$", tooltip: "Expencive", minPerPerson: 21, maxPerPerson: 50}
// ]
