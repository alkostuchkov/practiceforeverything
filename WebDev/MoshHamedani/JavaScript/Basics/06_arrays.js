// const numbers = [3, 4]
// // Add elements:
// // End
// numbers.push(5, 6)
// console.log(numbers)
// // Beginning
// numbers.unshift(1, 2)
// console.log(numbers)
// // Middle
// console.log(numbers.splice(2, 0, "a", "b"))
// console.log(numbers)

// // Finding elements (primitives)
// const numbers = [1, 2, 3, 1, 4]
// console.log(numbers.indexOf("a"))
// console.log(numbers.indexOf(1))
// console.log(numbers.indexOf(1, 2))
// console.log(numbers.lastIndexOf(1))
// console.log(numbers.indexOf(1) !== -1)
// console.log(numbers.includes(1))

// // Finding elements (reference types)
// const courses = [
//   {id: 1, name: "a"},
//   {id: 2, name: "b"}
// ]
// // const course = courses.find(course => course.name === "a")
// // console.log(course)
// const courseIndex = courses.findIndex(course => course.name === "a")
// console.log(courseIndex)

// // Removing elements
// const numbers = [2, 2, 3, 1, 4]
// // End
// // numbers.push()
// console.log(numbers.pop())
// console.log(numbers)
// // Beginning
// // numbers.unshift()
// console.log(numbers.shift())
// console.log(numbers)
// // Middle
// // numbers.splice()
// console.log(numbers.splice(2, 2))
// console.log(numbers)

// // Emptying an array
// let numbers = [2, 2, 3, 1, 4]
// let another = numbers
// // 1
// numbers = []
// console.log(another)

// // 2!
// numbers.length = 0
// console.log(numbers)
// console.log(another)

// // 3
// numbers.splice(0, numbers.length)
// console.log(numbers)
// console.log(another)

// // 4
// while (numbers.length > 0)
//   numbers.pop()

// console.log(numbers)
// console.log(another)

// // Combining Arrays
// const first = [1, 2, 3]
// const second = [4, 5, 6]

// const combined = first.concat(second)
// console.log(combined)

// // Slicing Arrays
// const sliced = combined.slice(2, 4)
// console.log(sliced)

// const objArray = [
//   {name: "John", age: 14},
//   {name: "Max", age: 11}
// ]
// const slicedArray = objArray.slice()
// console.log(objArray)
// console.log(slicedArray)
// slicedArray[0].name = "Jonny"
// console.log(objArray)
// console.log(slicedArray)

// // Spread operator
// const first = [{id: 1}]
// const second = [4, 5, 6]
// const spreaded = [...first, ...second]

// console.log(spreaded)
// spreaded[0].id = 10
// console.log(spreaded)
// console.log(first)

// // Iterating an Array
// const arr = [1, 2, 3]
// for (let item of arr) {
// console.log(item)
// }
// arr.forEach((item, index, array) => console.log(index, item, array))
// for (let i in arr) {
//   console.log(i, arr[i])
// }

// // Joining Arrays
// const arr = [1, 2, 3]
// const joined = arr.join(":")
// // const joined = arr.join()
// console.log(joined)
// console.log(typeof joined)
// // Split (String's method)
// const message = "This is my message"
// const splitted = message.split(" ")
// console.log(splitted)
// const joinedAfterSplit = splitted.join("::")
// console.log(joinedAfterSplit)

// // Sorting an Array
// const arr = [2, 4, 1, 3]
// arr.sort()
// console.log(arr)
// // arr.sort((first, second) => second - first)
// // console.log(arr)
// arr.reverse()
// console.log(arr)

// const objArray = [
//   {name: "John", age: 14},
//   {name: "alex", age: 25},
//   {name: "Max", age: 11}
// ]
// objArray.sort((a, b) => a.age - b.age)
// console.log(objArray)
// objArray.sort((a, b) => {
//   const nameA = a.name.toLowerCase()
//   const nameB = b.name.toLowerCase()
//   if (nameA < nameB) return -1
//   if (nameA > nameB) return 1
//   return 0
// })
// console.log(objArray)

// // Testing elements of an Array
// const arr = [2, 4, -1, 3]
// console.log(arr.every(item => item > 0))
// console.log(arr.some(item => item <= 0))

// const items = arr.filter(item => item >= 0).map(item => ({value: item}))
// console.log(items)

// // Exercises
// // 1
// function arrayFromRange(min, max) {
//   const arr = []
//   for (let i = min; i <= max; i++) {
//     arr.push(i)
//   }
//   return arr
// }
// console.log(arrayFromRange(-10, 2))

// // 2
// function myIncludes(array, serachElement) {
//   for (let item of array)
//     if (item === serachElement)
//       return true

//   return false
// }
// console.log(myIncludes([1, 2, 3, 4], -1))

// // 3
// // function exceptValues(array, excluded) {
// //   for (let excludeItem of excluded) {
// //     let excludeIndex = array.indexOf(excludeItem)
// //     while (excludeIndex !== -1) {
// //       array.splice(excludeIndex, 1)
// //       excludeIndex = array.indexOf(excludeItem)
// //     }
// //   }
// // }

// // exceptValues(numbers, [1, 5, 3])
// // console.log(numbers)

// function exceptValues(array, excluded) {
//   const outputArray = []
//   for (let item of array)
//     // if (excluded.indexOf(item) === -1)
//     if (!excluded.includes(item))
//       outputArray.push(item)

//   return outputArray
// }

// const numbers = [3, 1, 2, 3, 1, 4, 5, 3, 6, 7, 1]
// console.log(exceptValues(numbers, [1, 5, 3]))

// // 4
// function moveItem(array, index, offset) {
//   const outputArray = [...array]
//   let newIndex = index + offset
//   if (newIndex < 0 || newIndex >= outputArray.length) {
//     console.error(`Invalid index: ${newIndex}.`)
//     return
//   }
//   let movingItem = outputArray.splice(index, 1)
//   outputArray.splice(newIndex, 0, ...movingItem)

//   return outputArray
// }

// const arr = [1, 2, 3, 4]
// console.log(moveItem(arr, 2, 1))
// console.log(arr)

// // 5
// // function countOccurrences(array, serachElement) {
// //   let count = 0
// //   for (let item of array)
// //     if (item === serachElement) count++

// //   return count
// // }

// function countOccurrences(array, serachElement) {
//   return array.reduce((accumulator, item) => {
//     const occurrences = (item === serachElement) ? 1 : 0

//     return accumulator + occurrences
//     // if (item === serachElement)
//     //   accumulator++

//     // return accumulator
//   }, 0)
// }

// const arr = [1, 2, 3, 4, 1, 3, 3]
// console.log(countOccurrences(arr, 1))

// 6
// function getMax(array) {
//   if (!array.length) return undefined
//   let max = array[0]
//   for (let i = 1; i < array.length; i++)
//     if (array[i] > max)
//       max = array[i]

//   return max
// }

// function getMax(array) {
//   if (!array.length) return undefined

//   return array.reduce((a, b) => (a > b ? a : b))
//   // if default for accumulator is not defined, it'll be the first item of the array!!!
// }

// console.log(getMax([2, 1, 4, 3, 7, 0]))
// console.log(getMax([]))

// // 7
// // All the movies in 2018 with rating > 4
// // Sort them by their rating
// // Descending order
// // Pick their title
// const movies = [
//   {title: "a", year: 2018, rating: 4.5},
//   {title: "b", year: 2018, rating: 4.7},
//   {title: "c", year: 2018, rating: 3},
//   {title: "d", year: 2017, rating: 4.5}
// ]

// movies
//   .filter(movie => (movie.year === 2018) && (movie.rating > 4))
//   .sort((a, b) => b.rating - a.rating)
//   .map(movie => console.log(movie.title))
