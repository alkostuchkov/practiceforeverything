// // Bitwise operators
// // 00000111
// // 00000110
// // 00000101
// // 00000100
// // 00000011
// // 00000010
// // 00000001
// // 00000000
// console.log(7 & 7)
// console.log(6 & 7)
// console.log(5 & 7)
// console.log(4 & 7)
// console.log(3 & 7)
// console.log(2 & 7)
// console.log(1 & 7)
// console.log(0 & 7)

// const readPermission = 4
// const writePermission = 2
// const executePermission = 1
// let userPermission = 0
// userPermission = userPermission | readPermission | writePermission
// console.log(userPermission)
// console.log(userPermission & executePermission)
// console.log(userPermission & writePermission)
// let isPermission =
//   userPermission & executePermission ? "yes permissions" : "no permissions"
// console.log(isPermission)

// let hour = 10
// if (hour >= 6 && hour < 12) {
//   console.log("Good morning!")
// } else if (hour >= 12 && hour < 18) {
//   console.log("Good afternoon!")
// } else {
//   console.log("Good everning!")
// }

// let role = "admin"
// switch (role) {
//   case "admin":
//     console.log("admin")
//     break
//   case "moderator":
//     console.log("moderator")
//     break
//   case "guest":
//     console.log("guest")
//     break
//   default:
//     console.log("Unknown")
//     break
// }

// for (let i = 0; i > 0; i++) console.log("hello")

// const colors = ["red", "yellow", "green", "blue"]
// for (let index in colors) console.log(index, colors[index])

// for (let color of colors) console.log(color)

// function getMax(num1, num2) {
//   return num1 >= num2 ? num1 : num2
// }
// console.log(getMax(20, 10))
// console.log(getMax(20, 30))
// console.log(getMax(40, 40))

// function isLandscape(width, height) {
//   return width > height
//   // return width > height ? true : false
// }
// console.log(isLandscape(500, 300))
// console.log(isLandscape(200, 400))

// // FizzBuzz
// function fizzBuzz(number) {
//   if (typeof number !== "number") return NaN
//   else if (number % 3 === 0 && number % 5 === 0) return "FizzBuzz"
//   else if (number % 3 === 0) return "Fizz"
//   else if (number % 5 === 0) return "Buzz"
//   else return number
// }
// console.log(fizzBuzz("7"))
// console.log(fizzBuzz(3))
// console.log(fizzBuzz(5))
// console.log(fizzBuzz(15))
// console.log(fizzBuzz(7))
// console.log(fizzBuzz(12))
// console.log(fizzBuzz(10))
// console.log(fizzBuzz(8))

// // Demerit points
// // Speed limit = 70
// // 5 -> 1 point
// // Math.floor(1.3)
// // 12 points -> suspended
// function speedCheck(speed) {
//   const speedLimit = 70
//   const kmPerPoint = 5

//   if (speed < speedLimit + kmPerPoint) {
//     console.log("Ok")
//     return
//   }

//   const demeritPoints = Math.floor((speed - speedLimit) / kmPerPoint)
//   if (demeritPoints >= 12) console.log("Licence suspended")
//   else console.log(`Points: ${demeritPoints}`)
// }
// speedCheck(71)

// // Even Odd
// function showNumbers(limit) {
//   for (let i = 0; i <= limit; i++) {
//     const message = i % 2 ? "ODD" : "EVEN"
//     console.log(i, message)
//   }
// }
// showNumbers(10)

// // Count Truthy
// function countTruthy(array) {
//   let count = 0
//   for (let item of array) {
//     if (item) count++
//   }
//   return count
// }
// const arr = [0, 1, 2, undefined, "", null, false, true, NaN]
// console.log(countTruthy(arr))

// // String Properties
// function showProperties(obj) {
//   // const keys = Object.keys(obj)
//   // console.log(keys)
//   for (let key in obj) {
//     if (obj.hasOwnProperty(key) && typeof obj[key] === "string")
//       console.log(key, obj[key])
//     // if (typeof obj[key] === "string") console.log(key, obj[key])
//   }
// }

// const person = {
//   name: "John",
//   age: 14,
//   job: "Student",
//   index: 1234
// }
// showProperties(person)

// // Grades
// function calculateGrades(marks) {
//   const avarage = calculateAverage(marks)

//   if (avarage < 60) return "F"
//   if (avarage < 70) return "D"
//   if (avarage < 80) return "C"
//   if (avarage < 90) return "B"
//   return "A"
// }

// function calculateAverage(array) {
//   let summa = 0
//   for (let value of array) summa += value

//   return summa / array.length
// }

// const marks = [80, 80, 50]
// console.log(calculateGrades(marks))

// // Show stars
// function showStars(rows) {
//   for (let row = 1; row <= rows; row++) {
//     let stars = ""
//     for (let cols = 1; cols <= row; cols++) {
//       stars += "*"
//     }
//     console.log(stars)
//   }
// }
// showStars(5)

// // Show primes (простые числа)
// // factor (множитель, коэффициент)
// function showPrimes(limit) {
//   for (let number = 2; number <= limit; number++)
//     if (isPrime(number)) console.log(number)
// }

// function isPrime(number) {
//   for (let factor = 2; factor < number; factor++)
//     if (number % factor === 0) return false

//   return true
// }
// showPrimes(20)

// // function showPrimes(limit) {
// //   for (let number = 2; number <= limit; number++) {
// //     let isPrime = true
// //     for (let factor = 2; factor < number; factor++) {
// //       if (number % factor === 0) {
// //         isPrime = false
// //         break
// //       }
// //     }
// //     if (isPrime) console.log(number)
// //   }
// // }
// // showPrimes(20)
