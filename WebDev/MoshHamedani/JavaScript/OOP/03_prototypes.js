// // Property Descriptors
// function Person() {
//   this.firstName = "John",
//   Object.defineProperty(this, "firstName", {
//     enumerable: false,
//     writable: false,
//     configurable: false
//   })
// }

// const person = new Person()
// console.log(person.firstName)
// person.firstName = "Max"
// console.log(person.firstName)
// delete person.firstName
// console.log(person.firstName)

// // Prototype vs Instance Members
// function Circle(radius) {
//   // Instance members
//   this.radius = radius

//   this.move = function () {
//     console.log("move")
//   }
// }
// // Prototype members
// Circle.prototype.draw = function () {
//   this.move()
//   console.log("draw")
// }

// Circle.prototype.toString = function () {
//   console.log("Circle with radius " + this.radius)
// }

// const c1 = new Circle(1)
// const c2 = new Circle(5)

// // Returns instance members
// // console.log(Object.keys(c1))

// // Returns all members (instance (own) and prototype)
// for (let key in c1) {
//   console.log(key)
// }
// console.log(c1.hasOwnProperty("move"))
// console.log(c1.hasOwnProperty("draw"))

// Exercise
function Stopwatch() {
  let duration = 0
  let isStarted = false
  let startTime = null
  let endTime = null

  Object.defineProperty(this, "duration", {
    get: function () {
      if (endTime > startTime) duration += (endTime - startTime) / 1000
      else if (startTime)
        console.log(
          "Stopwatch is still running. Stop Stopwatch to see new duration."
        )

      return duration
    },

    set: function (value) {
      duration = value
    }
  })

  Object.defineProperty(this, "isStarted", {
    get: function () {
      return isStarted
    }
  })

  Object.defineProperty(this, "startTime", {
    get: function () {
      return startTime
    }
  })

  Object.defineProperty(this, "endTime", {
    get: function () {
      return endTime
    }
  })
}

Stopwatch.prototype.start = function () {
  if (this.isStarted) throw new Error("Stopwatch has already started.")
  this.isStarted = true
  this.startTime = new Date().getTime()
  console.log("Started...")
}

Stopwatch.prototype.stop = function () {
  if (!this.isStarted) throw new Error("Stopwatch has already stopped.")
  this.isStarted = false
  this.endTime = new Date().getTime()
  console.log("Stopped...")
}

Stopwatch.prototype.reset = function () {
  this.isStarted = false
  this.startTime = null
  this.endTime = null
  this.duration = 0
}
