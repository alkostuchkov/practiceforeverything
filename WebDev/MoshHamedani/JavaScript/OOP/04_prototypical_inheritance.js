// function Shape(color) {
//   this.color = color
// }

// Shape.prototype.duplicate = function () {
//   console.log("duplicate")
// }

// function extend(Child, Parent) {
//   Child.prototype = Object.create(Parent.prototype)
//   Child.prototype.constructor = Child
// }

// function Circle(radius, color) {
//   Shape.call(this, color)
//   this.radius = radius
// }

// extend(Circle, Shape)

// Circle.prototype.duplicate = function () {
//   // Shape.prototype.duplicate.call(this)

//   console.log("duplicate circle")
// }
// // Circle.prototype.constructor = Circle
// // new Circle.prototype.constructor() => new Circle()

// Circle.prototype.draw = function () {
//   console.log("draw")
// }

// function Square(size) {
//   this.size = size
// }

// extend(Square, Shape)

// Square.prototype.duplicate = function () {
//   console.log("duplicate square")
// }

// const s = new Shape("green")
// const c = new Circle(5, "red")
// // const c2 = new Circle.prototype.constructor(1)
// const sq = new Square(10)

// const shapes = [new Circle(25, "blue"), new Square(20)]

// for (let shape of shapes) {
//   shape.duplicate()
// }

// // Composition
// function composite(target, ...sources) {
//   Object.assign(target.prototype, ...sources)
// }

// const canEat = {
//   eat: function () {
//     this.hunger--
//     console.log("eating")
//   }
// }

// const canWalk = {
//   walk: function () {
//     console.log("walking")
//   }
// }

// const canSwim = {
//   swim() {
//     console.log("swimming")
//   }
// }

// function Person() {}

// function Dog() {}

// function Fish() {}

// composite(Person, canEat, canWalk)
// composite(Dog, canEat, canSwim)
// composite(Fish, canEat, canSwim)
// // Object.assign(Person.prototype, canEat, canWalk)
// // const person = Object.assign({}, canEat, canWalk)
// // const person = {
// //   ...canEat,
// //   ...canWalk
// // }

// // const fish = {
// //   ...canEat,
// //   ...canSwim
// // }

// Execrcises
function HtmlElement() {
  this.click = function () {
    console.log("clicked")
  }
}

HtmlElement.prototype.focus = function () {
  console.log("focused")
}

function HtmlSecectElement(items = []) {
  this.items = items

  this.addItem = function (item) {
    this.items.push(item)
  }

  this.removeItem = function (item) {
    const index = items.indexOf(item)
    if (index !== -1) this.items.splice(index, 1)
  }

  this.render = function () {
    return `
<select>${this.items.map(item => `
  <option>${item}</option>`).join("")}
</select>`
  }
}

// HtmlSecectElement.prototype = Object.create(HtmlElement.prototype)
HtmlSecectElement.prototype = new HtmlElement()
HtmlSecectElement.prototype.constructor = HtmlSecectElement

function HtmlImageElement(src) {
  this.src = src

  this.render = function () {
    return `<img scr="${this.src}" />`
  }
}

HtmlImageElement.prototype = new HtmlElement()
HtmlImageElement.prototype.constructor = HtmlImageElement

const elements = [
  new HtmlSecectElement([1, 2, 3, 4, 5, 6]),
  new HtmlSecectElement(),
  new HtmlImageElement("https://")
]

for (let element of elements) 
  console.log(element.render())
