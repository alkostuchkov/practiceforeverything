// function Circle(radius) {
//   this.radius = radius

//   this.move = function () {}
// }

// Circle.prototype.draw = function () {
//   console.log("draw")
// }

// class CircleClass {
//   static a = 5

//   constructor(radius) {
//     this.radius = radius

//     this.move = function () {}
//   }

//   draw() {
//     console.log("draw")
//   }

//   static parse(str) {
//     const radius = JSON.parse(str).radius
//     return new CircleClass(radius)
//   }
// }

// const circle = CircleClass.parse('{"radius": 77}')
// console.log(circle)

// // Private fields and methods by Symbol()
// const _radius = Symbol()
// const _draw = Symbol()

// class Circle {
//   constructor(radius) {
//     this[_radius] = radius
//   }

//   [_draw]() {
//     console.log(`draw radius ${this[_radius]}.`)
//   }

//   showDraw() {
//     this[_draw]()
//   }
// }

// const c = new Circle(1)
// console.log(c[_radius])

// // Private fields and methods by WeakMap
// const _radius = new WeakMap()
// const _size = new WeakMap()
// const _move = new WeakMap()

// class Circle {
//   constructor(radius, size) {
//     _radius.set(this, radius)
//     _size.set(this, size)

//     // _move.set(this, function () {
//     //   console.log("move", this)
//     // })

//     _move.set(this, () => {
//       console.log("move", this)
//     })
//   }

//   draw() {
//     console.log("draw radius", _radius.get(this))
//     console.log("draw size", _size.get(this))
//     _move.get(this)()
//   }
// }

// const c = new Circle(10, 20)
// c.draw()

// // Getters and Setters
// const _radius = new WeakMap()

// class Circle {
//   constructor(radius) {
//     _radius.set(this, radius)
//   }

//   get radius() {
//     return _radius.get(this)
//   }

//   set radius(newRadius) {
//     if (newRadius <= 0)
//       throw new Error("Invalid radius.")

//     _radius.set(this, newRadius)
//   }
// }

// const c = new Circle(7)
// console.log(c.radius)

// // Inheritance
// class Shape {
//   constructor(color) {
//     this.color = color
//   }

//   draw() {
//     console.log("draw with a color", this.color)
//   }
// }

// class Circle extends Shape {
//   constructor(radius, color) {
//     super(color)
//     this.radius = radius
//   }

//   draw() {
//     super.draw()
//     console.log("draw circle with a radius", this.radius)
//   }
// }

// const c = new Circle(77, "red")

// // Exercises
// const _stack = new WeakMap()
// const _count = new WeakMap()

// class Stack {
//   constructor() {
//     _count.set(this, 0)
//     _stack.set(this, [])
//   }

//   get count() {
//     return _count.get(this)
//   }

//   push(value) {
//     _stack.get(this).push(value)
//     _count.set(this, _count.get(this) + 1)
//   }

//   pop() {
//     if (_count.get(this) === 0) throw new Error("Stack is empty.")

//     _count.set(this, _count.get(this) - 1)

//     return _stack.get(this).pop()
//   }

//   peek() {
//     // Only returns the top value, not remove
//     // Top is this.count - 1

//     if (_count.get(this) === 0) throw new Error("Stack is empty.")

//     return _stack.get(this)[_count.get(this) - 1]
//   }
// }

const _stack = new WeakMap()

class Stack {
  constructor() {
    _stack.set(this, [])
  }

  get count() {
    return _stack.get(this).length
  }

  push(value) {
    _stack.get(this).push(value)
  }

  pop() {
    if (_stack.get(this).length === 0) throw new Error("Stack is empty.")

    return _stack.get(this).pop()
  }

  peek() {
    // Only returns the top value, not remove
    if (_stack.get(this).length === 0) throw new Error("Stack is empty.")

    return _stack.get(this)[_stack.get(this).length - 1]
  }
}
