// // Factories
// function createCircle(radius) {
//   return {
//     radius,
//     draw() {
//       console.log(`Draw with a radius ${this.radius}.`)
//     }
//   }
// }
// const circle1 = createCircle(1)
// circle1.draw()

// // Constructor
// function Circle(radius) {
//   this.radius = radius
//   // this.defaultLocation = {x: 0, y: 0}
//   // hidding a member from the outside
//   let defaultLocation = {x: 0, y: 0} // now this is a local variable accessiable inside Circle

//   // hidding a function inside
//   let computeOptimumLocation = function (factor) {
//     // ...
//   }

//   this.draw = function () {
//     computeOptimumLocation(0.1)
//     console.log(`Draw with a radius ${this.radius}.`)
//   }
// }
// const circle2 = new Circle(2)
// circle2.draw()

// // Getters and Setters
// function Circle(radius) {
//   this.radius = radius
//   // this.defaultLocation = {x: 0, y: 0}
//   // hidding a member from the outside
//   let defaultLocation = {x: 0, y: 0} // now this is a local variable accessiable inside Circle

//   Object.defineProperty(this, "defaultLocation", {
//     get: function () {
//       return defaultLocation
//     },
//     set: function (value) {
//       if (!value.x || !value.y)
//         throw new Error("Invalid location.")

//       defaultLocation = value
//     }
//   })
//   this.draw = function () {
//     console.log(`Draw with a radius ${this.radius}.`)
//   }
// }
// const circle3 = new Circle(7)
// console.log(circle3.defaultLocation)
// circle3.defaultLocation = {x: 2, y: 3}
// console.log(circle3.defaultLocation)

// Exercices
// // Stopwatch
function Stopwatch() {
  let duration = 0
  let isStarted = false
  let startTime = null
  let endTime = null

  Object.defineProperty(this, "duration", {
    get: function () {
      if (endTime > startTime) duration += (endTime - startTime) / 1000
      else if (startTime)
        console.log(
          "Stopwatch is still running. Stop Stopwatch to see new duration."
        )
      // console.log(`startTime = ${startTime}, endTime = ${endTime}`)

      return duration
    }
  })

  this.start = function () {
    if (isStarted) throw new Error("Stopwatch has already started.")
    isStarted = true
    startTime = new Date().getTime()
    console.log("Started...")
    // console.log(startTime)
  }

  this.stop = function () {
    if (!isStarted) throw new Error("Stopwatch has already stopped.")
    isStarted = false
    endTime = new Date().getTime()
    console.log("Stopped...")
    // console.log(endTime)
  }

  this.reset = function () {
    isStarted = false
    startTime = null
    endTime = null
    duration = 0
  }
}
