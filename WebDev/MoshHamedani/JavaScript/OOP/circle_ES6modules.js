const _radius = new WeakMap()

// Public Interface
class Circle {
  constructor(radius) {
    _radius.set(this, radius)
  }

  draw() {
    console.log(`Circle with a radius ${_radius.get(this)}.`)
  }
}

export {Circle}
// export default Circle
