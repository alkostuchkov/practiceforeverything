// // CommonJS modules
// const Circle = require("./circle_commonJS")

// ES6 Modules
import {Circle} from "./circle"
// import {Circle} from "./circle.js"
// import Circle from "./circle_ES6modules.js"

const c = new Circle(10)
c.draw()
