// import "./App.css";
// import Counter from "./components/Counter";
import React, {useState} from "react";
import Counters from "./components/Counters";
import NavBar from "./components/NavBar";

function App() {
  const [countersList, setCountersList] = useState([
    {id: 1, value: 0},
    {id: 2, value: 2},
    {id: 3, value: 3},
    {id: 4, value: 4}
  ]);

  function handleDelete(counterId) {
    const remainCounters = countersList.filter(
      counter => counter.id !== counterId
    );
    setCountersList(remainCounters);
    // console.log("Event handler called...", id);
  }

  function handleReset() {
    const resetedCounters = countersList.map(counter => {
      counter.value = 0;
      return counter;
    });
    setCountersList(resetedCounters);
  }

  function handleIncrement(counter) {
    const countersListCopy = [...countersList];
    const index = countersListCopy.indexOf(counter);
    countersListCopy[index] = {...counter}; // Deep copy of the counter.
    countersListCopy[index].value++;
    setCountersList(countersListCopy);
  }

  function handleDecrement(counter) {
    const countersListCopy = [...countersList];
    const index = countersListCopy.indexOf(counter);
    countersListCopy[index] = {...counter}; // Deep copy of the counter.
    countersListCopy[index].value--;
    setCountersList(countersListCopy);
  }

  return (
    <React.Fragment>
      <NavBar
        totalCounters={countersList.filter(counter => counter.value > 0).length}
      />
      <main className="container">
        <Counters
          countersList={countersList}
          onDelete={handleDelete}
          onIncrement={handleIncrement}
          onDecrement={handleDecrement}
          onReset={handleReset}
        />
      </main>
    </React.Fragment>
  );
}

export default App;
