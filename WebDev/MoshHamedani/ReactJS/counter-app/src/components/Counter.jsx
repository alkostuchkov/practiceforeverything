import React from "react";

function Counter(props) {
  function getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  function formatCount() {
    return props.counter.value === 0 ? "Zero" : props.counter.value;
  }

  return (
    <div className="row">
      <div className="col-1">
        <span className={getBadgeClasses()}>{formatCount()}</span>
      </div>
      <div className="col">
        <button
          className="btn btn-secondary btn-sm"
          onClick={() => props.onIncrement(props.counter)}
        >
          +
        </button>
        <button
          className="btn btn-secondary btn-sm m-2"
          onClick={() => props.onDecrement(props.counter)}
          disabled={props.counter.value === 0}
        >
          -
        </button>
        <button
          onClick={() => props.onDelete(props.counter.id)}
          className="btn btn-danger btn-sm"
        >
          X
        </button>
      </div>
    </div>
  );
}

export default Counter;

// // const [tags, setTags] = useState(["tag1", "tag2", "tag3"]);
// const [tags, setTags] = useState([]);

// function renderTags() {
//   return tags.length === 0 ? (
//     <p>There are no tags!</p>
//   ) : (
//     <ul>
//       {tags.map(tag => (
//         <li key={tag}>{tag}</li>
//       ))}
//     </ul>
//   );
// }

//   return (
//   <div>
//     <span className={getBadgeClasses()}>{formatCount()}</span>
//     <button onClick={handleEncrement} className="btn btn-secondary btn-sm">
//       Increment
//     </button>
//     <button onClick={handleDecrement} className="btn btn-secondary btn-sm">
//       Decrement
//     </button>
//     <p>{tags.length === 0 && "Please, create a new tag!"}</p>
//     {renderTags()}
//   </div>
// );
