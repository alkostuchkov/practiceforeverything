import React from "react";
import Counter from "./Counter";

function Counters(props) {
  return (
    <div>
      <button className="btn btn-primary btn-sm m-2" onClick={props.onReset}>
        Reset
      </button>
      {props.countersList.map(counter => (
        <Counter
          key={counter.id}
          // id={counter.id}
          // value={counter.value}
          counter={counter}
          onDelete={props.onDelete}
          onIncrement={props.onIncrement}
          onDecrement={props.onDecrement}
        />
      ))}
    </div>
  );
}

export default Counters;
