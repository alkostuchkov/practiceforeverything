import React, {useEffect, useState} from "react";
import {getMovies} from "../services/fakeMovieService";
import Pagination from "./common/Pagination";
import {paginate} from "../utils/paginate";
import ListGroup from "./ListGroup";
import {getGenres} from "../services/fakeGenreService";
import MoviesTable from "./MoviesTable";
import _ from "lodash";

function Movies() {
  const [allMovies, setAllMovies] = useState([]);
  const [allGenres, setAllGenres] = useState([]);
  const [pageSize, setPageSize] = useState(4);
  const [currentPage, setCurrentPage] = useState(1);
  // const [allMovies, setAllMovies] = useState(getMovies());
  // const [allGenres, setAllGenres] = useState([
  //   {_id: "", name: "All genres"},
  //   ...getGenres()
  // ]);
  // const [selectedGenre, setSelectedGenre] = useState(allGenres[0]);
  const [selectedGenre, setSelectedGenre] = useState(null);
  const [sortColumn, setSortColumn] = useState({path: "title", order: "asc"});

  useEffect(() => {
    setAllMovies(getMovies());
    setAllGenres([{_id: 0, name: "All genres"}, ...getGenres()]);
    setSelectedGenre(allGenres[0]);
    console.log("useEffect...");
  }, []);

  function handleDelete(movie) {
    const movies = allMovies.filter(m => m._id !== movie._id);
    setAllMovies(movies);
  }

  function handleLike(movie) {
    const movies = [...allMovies];
    const index = allMovies.indexOf(movie);
    movies[index] = {...movie};
    movies[index].liked = !movie.liked;
    // console.log(allMovies[index]);
    // console.log(movies[index]);
    setAllMovies(movies);
  }

  function handlePageChange(page) {
    setCurrentPage(page);
  }

  function handleGenreSelect(genre) {
    setSelectedGenre(genre);
    setCurrentPage(1);
  }

  function handleSort(sortColumn) {
    setSortColumn(sortColumn);
  }

  const filteredMoviesByGenre =
    selectedGenre && selectedGenre._id
      ? allMovies.filter(movie => movie.genre._id === selectedGenre._id)
      : allMovies;

  const sortedMovies = _.orderBy(
    filteredMoviesByGenre,
    [sortColumn.path],
    [sortColumn.order]
  );

  const moviesCounter = allMovies.length;
  if (moviesCounter === 0) return <p>There are no movies in the database.</p>;

  // const movies = paginate(filteredMoviesByGenre, currentPage, pageSize);
  const movies = paginate(sortedMovies, currentPage, pageSize);

  return (
    <div className="row">
      <div className="col-3">
        <ListGroup
          items={allGenres}
          // textProperty="name"  // set as defaultProps
          // valueProperty="_id"  // set as defaultProps
          selectedItem={selectedGenre}
          onItemSelect={handleGenreSelect}
        />
      </div>
      <div className="col">
        <p>Showing {filteredMoviesByGenre.length} movie(s) in the database.</p>
        <MoviesTable
          movies={movies}
          sortColumn={sortColumn}
          onDelete={handleDelete}
          onLike={handleLike}
          onSort={handleSort}
        />
        <Pagination
          itemsCount={filteredMoviesByGenre.length}
          pageSize={pageSize}
          currentPage={currentPage}
          onPageChange={handlePageChange}
        />
      </div>
    </div>
  );
}

export default Movies;
