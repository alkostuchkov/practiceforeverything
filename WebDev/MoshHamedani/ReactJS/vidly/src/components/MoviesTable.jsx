import React from "react";
import TableHeader from "./common/TableHeader";
import TableBody from "./common/TableBody";
import Like from "./common/Like";

function MoviesTable(props) {
  const {movies, onDelete, onLike, onSort, sortColumn} = props;
  const columns = [
    {label: "Title", path: "title"},
    {label: "Genre", path: "genre.name"},
    {label: "Stock", path: "numberInStock"},
    {label: "Rate", path: "dailyRentalRate"},
    {
      key: "like",
      content: movie => (
        <Like liked={movie.liked} onClick={() => onLike(movie)} />
      )
    },
    {
      key: "delete",
      content: movie => (
        <button
          onClick={() => onDelete(movie)}
          className="btn btn-outline-danger btn-sm"
        >
          Delete
        </button>
      )
    }
  ];

  return (
    <table className="table">
      <TableHeader columns={columns} sortColumn={sortColumn} onSort={onSort} />
      <TableBody items={movies} columns={columns} />
    </table>
  );
}

export default MoviesTable;
