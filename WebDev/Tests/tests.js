// function stopWatch() {
//   const startTime = Date.now();
//   console.log(startTime);

// const {isInteger} = require("prettier")

//   function getDelay() {
//     console.log(startTime);
//     const elapsedTime = Date.now() - startTime;
//     console.log(elapsedTime);
//   }

//   return getDelay;
// }

// const timer = stopWatch();
// for (let i = 0; i < 1000000; i++) {
//   const foo = Math.random() * 1000;
// }
// timer();

// const person = {
//   name: "John",
//   age: 13,
//   address: {
//     city: "New York",
//     zip: 112233
//   }
// };

// // function destr({...obj}) {
// function destr({address, ...obj}) {
//   // function destr(obj) {
//   console.log(obj);
//   console.log(address);
// }

// destr(person);

// const {...p} = person;
// console.log({person});

// let s1 = "";
// s1 += "Hello";
// s1 += ", World";
// s1 += "!";
// s1[0] += "L";
// console.log(s1);

// class Test {
//   constructor() {
//     this.name = "Alex";
//     // this.myFunFun = this.myFun;
//   }

//   funArrow = () => {
//     console.log(this);
//     console.log(this.name);
//   };

//   myFun() {
//     console.log(this);
//     console.log(this.name);
//   }
// }

// const test = new Test();
// test.myFun();
// test.funArrow();

// function test(obj, number, per) {
//   obj.push(77);
//   number = 88;
//   per.name = "Maxim";
//   per.age = 12;
//   console.log("In function...", obj, number, per);
// }

// const number = 0;
// const arr = [1, 2, 3, 4];
// const person = {
//   name: "john",
//   age: 13
// };
// console.log("Before function...", arr, number, person);
// test(arr, number, person);
// console.log("After function...", arr, number, person);
// console.table(person);
// console.log(person);
// console.dir(person);

// const arr = [6, 7, 8];
// const arr2 = [1, 2, 3, 4, 5, ...arr];
// console.log(arr);
// console.log(arr2);

// const strName = "string name";
// const symName = Symbol("symbol name");
// console.log(typeof symName);
// const person = {
//   symName: "john",
//   age: 13
// };
// console.log(person);

// class Hero {
//   static sayHowdy() {
//     console.log("Howdy!");
//   }

//   constructor(name) {
//     this._name = name;
//   }

//   get name() {
//     return this._name;
//   }

//   set name(newName) {
//     if (newName === "" || newName === undefined) {
//       this._name = "Super hero";
//     } else {
//       this._name = newName;
//     }
//   }
// }

// const hero = new Hero("John");
// console.log(hero.name);
// // hero.name = "Maxim";
// let newName;
// hero.name = newName;
// console.log(hero.name);
// Hero.sayHowdy();

// const person = {
//   firstName: "John",
//   age: 14
// };

// const keys = Object.keys(person);
// for (let key of keys) {
//   console.log(key);
// }

// let sequence = {}
// for (let letter of "mississippi") {
//   // sequence[letter] = sequence[letter] ? sequence[letter] + 1 : 1
//   sequence[letter] = sequence[letter] ? ++sequence[letter] : 1
//   // sequence[letter] = sequence[letter]++ ? sequence[letter] : 1  // Wrong!!!
// }
// console.log(sequence)

// // 1
// const num = 42
// let result = num > 50 ? "num > 50" : "num < 50"
// console.log(result)

// // 2
// let variable
// result = variable || "variable is undefined"
// console.log(result)

// 3
// let x
// let y
// let z = 42
// let x, y, z = 42

// 4
// let isSame = true

// if (isSame === true) {}
// if (isSame) {}

// if (isSame !== true) {}
// if (!isSame) {}

// // Promises
// const p = new Promise((resolve, reject) => {
//   let success = true
//   console.log("Prepearing data...")
//   const backendData = {
//     field1: "field1",
//     modified: false
//   }
//   if (success) {
//     resolve(backendData)
//   } else {
//     reject("Some error")
//   }
// })
// // console.log(p)
// p.then(backendData => {
//   console.log(backendData)
//   return new Promise((resolve, reject) => {
//     backendData.modified = true
//     console.log("Modified data...")
//     let success = true
//     if (success) {
//       resolve(backendData)
//     } else {
//       reject("Error in Modified data")
//     }
//   })
//     .then(modifiedData => console.log(modifiedData))
//     .catch(err => console.log(err))
//     .finally(console.log("Just a finally for the second Promise"))
// })
//   .catch(err => {
//     console.log(err)
//   })
//   .finally(() => {
//     setTimeout(() => {
//       console.log("Just a finally for the first Promise")
//     }, 2000)
//   })

// const url = "https://jsonplaceholder.typicode.com/todos/1"
// fetch(url)
//   .then(response => response.json())
//   .then(data => console.log(data))
//   .catch(err => console.error(err))

// async function loadData() {
//   const res = await fetch("https://jsonplaceholder.typicode.com/todos/1")
//   const data = await res.json()
//   console.log(data)
// }
// loadData()

// const loadData = async () => {
//   try {
//     const res = await fetch("https://jsonplaceholder.typicode.com/todos/1")
//     console.log(res.status)
//     console.log(res.ok)
//     const data = await res.json()
//     console.log(data)
//     return data
//   } catch (err) {
//     console.error(err)
//   } finally {
//     console.log("finally...")
//   }
// }
// const data = loadData()
// console.log("data:", data)

// // __proto__ and prototype
// const man = {}
// const man2 = {}
// console.log(man.__proto__ === man2.__proto__)

// const age = 45
// const value = 50
// const name = "John"
// const job = "Manager"
// console.log(age.__proto__ === name.__proto__)
// console.log(age.__proto__ === value.__proto__)
// console.log(job.__proto__ === name.__proto__)

// console.log(man)
// String.prototype.sayHello = () => console.log("Hello")
// console.dir(String)
// console.log(String.prototype)
// console.log(Number.prototype)
// console.log(Object.prototype)

// console.log(name.__proto__ === String.prototype)
// console.log(man.__proto__ === Object.prototype)

// console.log(job.__proto__.__proto__)
// console.log(age.prototype)
// console.log(man.prototype)

// for (var i = 0; i < 10; i++) {
//   setTimeout(function () {
//     console.log(i)
//   }, 1000)
// }

// const goodsInCart = [
//   {
//     name: "JS",
//     price: 20
//   },
//   {
//     name: "Python",
//     price: 50
//   },
//   {
//     name: "C++",
//     price: 100
//   }
// ]
// const resultCost = goodsInCart.reduce((acc, good) => acc + good.price, 0)
// console.log(resultCost)

// ;(prev => console.log(prev))("some arg")
// ;[1, 2, 3].map((characters, index, arr) => console.log(characters, index, arr))

// function outer(fn) {
//   const arg = "Outer function"
//   fn(arg)
// }

// outer(message => console.log(message))

// // Array's methods:
// const characters = [
//   {
//     name: "Luke Skywalker",
//     height: 172,
//     mass: 77,
//     eyeColor: "blue",
//     gender: "male"
//   },
//   {
//     name: "Leia Organa",
//     height: 150,
//     mass: 49,
//     eyeColor: "brown",
//     gender: "female"
//   },
//   {
//     name: "Dart Vader",
//     height: 202,
//     mass: 136,
//     eyeColor: "yellow",
//     gender: "male"
//   },
//   {
//     name: "Leia Organa",
//     height: 150,
//     mass: 49,
//     eyeColor: "brown",
//     gender: "female"
//   },
//   {
//     name: "Anakin Skywalker",
//     height: 188,
//     mass: 84,
//     eyeColor: "blue",
//     gender: "male"
//   }
// ]

// //***MAP***
// //1. Get array of all names
// console.log("Map:")
// const names = characters.map(char => char.name)
// console.log(names)
// //2. Get array of all heights
// const heights = characters.map(char => char.height)
// console.log(heights)
// //3. Get array of objects with just name and height properties
// const namesAndHeights = characters.map(char => ({
//   name: char.name,
//   height: char.height
// }))
// console.log(namesAndHeights)
// //4. Get array of all first names
// const firstNames = characters.map(char => char.name.split(" ")[0])
// console.log(firstNames, "\n")

// //***REDUCE***
// //1. Get total mass of all characters
// console.log("Reduce:")
// const totalMass = characters.reduce((total, char) => total + char.mass, 0)
// console.log(totalMass)
// //2. Get total height of all characters
// const totalHeight = characters.reduce((total, char) => total + char.height, 0)
// console.log(totalHeight)
// //3. Get total number of characters by eye color
// const charactersByEyeColor = characters.reduce((total, char) => {
//   total[char.eyeColor] = total[char.eyeColor] ? ++total[char.eyeColor] : 1
//   return total
// }, {})
// console.log(charactersByEyeColor)
// //4. Get total number of characters in all the character names
// const totalCharactersInAllNames = characters.reduce(
//   (total, char) => total + char.name.length,
//   0
// )
// console.log(totalCharactersInAllNames, "\n")

// //***FILTER***
// //1. Get characters with mass greater than 100
// console.log("Filter:")
// const massGreater100 = characters.filter(char => char.mass > 100)
// console.log(massGreater100)
// //2. Get characters with height less than 200
// const heightLess200 = characters.filter(char => char.height < 200)
// console.log(heightLess200)
// //3. Get all male characters
// const allMale = characters.filter(char => char.gender === "male")
// console.log(allMale)
// //4. Get all female characters
// const allFemale = characters.filter(char => char.gender === "female")
// console.log(allFemale, "\n")

// //***SORT***
// //1. Sort by mass
// console.log("Sort:")
// const sortedByMass = characters.sort(
//   (prevChar, nextChar) => prevChar.mass - nextChar.mass // ascending order
// )
// console.log("By mass:", sortedByMass)
// //2. Sort by height
// const sortedByHeight = characters.sort(
//   (prevChar, nextChar) => nextChar.height - prevChar.height // descending order
// )
// console.log("By height:", sortedByHeight)
// //3. Sort by name
// const sortedByName = characters.sort((prevChar, nextChar) => {
//   if (prevChar.name < nextChar.name) return -1 // ascending order
//   return 1
// })
// console.log("By name:", sortedByName)
// //4. Sort by gender
// let sortedByGender = characters.sort((prevChar, nextChar) => {
//   if (prevChar.gender < nextChar.gender) return 1 // descending order
//   return -1
// })
// console.log("By gender:", sortedByGender)

// sortedByGender = characters.sort(prevChar => {
//   if (prevChar.gender === "female") return 1 // descending order
//   return -1
// })
// console.log("By gender:", sortedByGender, "\n")

// //***EVERY***
// //1. Does every character have blue eyes?
// console.log("Every:")
// const isEveryHasBlueEyes = characters.every(char => char.eyeColor === "blue")
// console.log("Every has blue eyes:", isEveryHasBlueEyes)
// //2. Does every character have mass more than 40?
// const isEveryHasMassMore40 = characters.every(char => char.mass > 40)
// console.log("Every has mass more than 40:", isEveryHasMassMore40)
// //3. Is every character shorter than 200?
// const isEveryShoter200 = characters.every(char => char.height < 200)
// console.log("Every is shoter than 200:", isEveryShoter200)
// //4. Is every character male?
// const isEveryMale = characters.every(char => char.gender === "male")
// console.log("Every is male:", isEveryMale, "\n")

// //***SOME***
// //1. Is there at least one male character?
// console.log("Some:")
// const isOneMale = characters.some(char => char.gender === "male")
// console.log("One is a male:", isOneMale)
// //2. Is there at least one character with blue eyes?
// const isOneHasBlueEyes = characters.some(char => char.eyeColor === "blue")
// console.log("One has blue eyes:", isOneHasBlueEyes)
// //3. Is there at least one character taller than 210?
// const isOneTaller210 = characters.some(char => char.height > 210)
// console.log("One is taller than 210:", isOneTaller210)
// //4. Is there at least one character that has mass less than 50?
// const isOneHasMassLess50 = characters.some(char => char.mass < 50)
// console.log("One has mass less than 50:", isOneHasMassLess50)

// // Factory function
// const createCircle = function (radius) {
//   return {
//     radius,
//     draw() {
//       console.log("draw")
//     }
//   }
// }
// const circle = createCircle(25)
// circle.draw()

// // Constructor
// function Circle(radius) {
//   this.radius = radius
//   this.draw = function () {
//     console.log("draw")
//   }
// }
// const circle2 = new Circle(30)
// circle2.draw()

// class CircleClass {
//   constructor(radius) {
//     this.radius = radius
//   }

//   draw() {
//     console.log("draw")
//   }
// }
// const circle3 = new CircleClass(77)
// circle3.draw()

// // Heigher functions
// function copyArrayAndDoSmth(fn, arr) {
//   const outputArr = []
//   for (let i = 0; i < arr.length; i++) {
//     outputArr.push(fn(arr[i]))
//   }
//   return outputArr
// }

// function squareNumber(num) {
//   return num ** 2
// }

// console.log(copyArrayAndDoSmth(squareNumber, [1, 2, 3, 4]))
// console.log(copyArrayAndDoSmth(num => num ** 3, [1, 2, 3, 4]))
// const arr = [1, 2, 3, 4]
// arr.forEach(item => item ** 2)
// console.log(arr)

// // Clousures
// function once(fn) {
//   let isCalled = false
//   let cachedResult

//   function innerOnce(...args) {
//     if (isCalled) return cachedResult

//     isCalled = true
//     cachedResult = fn(...args)

//     return cachedResult
//   }
//   return innerOnce
// }

// function addTwoOnce(num) {
//   return num + 2
// }
// const addTwo = once(addTwoOnce)
// console.log(addTwo(3, 4, 1))
// console.log(addTwo(7))

// const book = {
//   title: "Title",
//   author: {
//     firstName: "First Name",
//     surname: "Surname"
//   }
// }

// let surname = undefined

// // if (book) {
// //   if (book.author) {
// //     surname = book.author.surname
// //   }
// // }

// // surname = book && book.author && book.author.surname

// surname = book?.author?.surname // ES2020

// console.log(surname)

// const fs = require("fs")
// const f = fs.readFile(
//   "/home/alexander/Projects/PracticeForEverything/WebDev/Tests/tests.js",
//   err => console.log(err)
// )

// const genres = [
//   {_id: "5b21ca3eeb7f6fbccd471818", name: "Action"},
//   {_id: "5b21ca3eeb7f6fbccd471814", name: "Comedy"},
//   {_id: "5b21ca3eeb7f6fbccd471820", name: "Thriller"}
// ];

// function getGenres() {
//   return genres.filter(g => g);
// }

// console.log(getGenres());
// console.log(genres.map(genre => genre.name));

// const counters = [
//   {id: 1, value: 0},
//   {id: 2, value: 2},
//   {id: 3, value: 3},
//   {id: 4, value: 4}
// ];

// const countersCopy = [...counters];
// console.log(counters === countersCopy);
// console.log(counters[0] === countersCopy[0]);
// console.log();

// const countersDeepCopy = counters.map(counter => ({...counter}));
// console.log(counters === countersDeepCopy);
// console.log(counters[0] === countersDeepCopy[0]);
// console.log();

// const arr = [1, 2, 3];
// const arrCopy = [...arr];
// console.log(arr === arrCopy);

// // Codewars
// function squareDigits(num) {
//   let numStr = "";
//   for (let char of String(num)) {
//     numStr += String(parseInt(char) ** 2);
//   }
//   const result = parseInt(numStr);
//   console.log(result, typeof result);

//   // return ;
// }

// squareDigits(9119);
// squareDigits(2345);

const movies = [
  {
    _id: "5b21ca3eeb7f6fbccd471815",
    title: "Terminator",
    genre: {_id: "5b21ca3eeb7f6fbccd471818", name: "Action"},
    numberInStock: 6,
    dailyRentalRate: 2.5,
    publishDate: "2018-01-03T19:04:28.809Z",
    liked: false
  },
  {
    _id: "5b21ca3eeb7f6fbccd471816",
    title: "Die Hard",
    genre: {_id: "5b21ca3eeb7f6fbccd471818", name: "Action"},
    numberInStock: 5,
    dailyRentalRate: 2.5,
    liked: true
  },
  {
    _id: "5b21ca3eeb7f6fbccd471817",
    title: "Get Out",
    genre: {_id: "5b21ca3eeb7f6fbccd471820", name: "Thriller"},
    numberInStock: 8,
    dailyRentalRate: 3.5,
    liked: false
  },
  {
    _id: "5b21ca3eeb7f6fbccd471819",
    title: "Trip to Italy",
    genre: {_id: "5b21ca3eeb7f6fbccd471814", name: "Comedy"},
    numberInStock: 7,
    dailyRentalRate: 3.5,
    liked: true
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181a",
    title: "Airplane",
    genre: {_id: "5b21ca3eeb7f6fbccd471814", name: "Comedy"},
    numberInStock: 7,
    dailyRentalRate: 3.5,
    liked: false
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181b",
    title: "Wedding Crashers",
    genre: {_id: "5b21ca3eeb7f6fbccd471814", name: "Comedy"},
    numberInStock: 7,
    dailyRentalRate: 3.5,
    liked: false
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181e",
    title: "Gone Girl",
    genre: {_id: "5b21ca3eeb7f6fbccd471820", name: "Thriller"},
    numberInStock: 7,
    dailyRentalRate: 4.5,
    liked: false
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181f",
    title: "The Sixth Sense",
    genre: {_id: "5b21ca3eeb7f6fbccd471820", name: "Thriller"},
    numberInStock: 4,
    dailyRentalRate: 3.5,
    liked: false
  },
  {
    _id: "5b21ca3eeb7f6fbccd471821",
    title: "The Avengers",
    genre: {_id: "5b21ca3eeb7f6fbccd471818", name: "Action"},
    numberInStock: 7,
    dailyRentalRate: 3.5,
    liked: true
  }
];

const sortBy = "genre.name";
// const sortBy = "title";
const paths = sortBy.split(".");
console.log(paths);
movies.map(movie => console.log(movie[paths[0]][paths[1]]));
// movies.map(movie => console.log(movie[paths[0]]));
// const sortBy = "genre";
// movies.map(movie => console.log(movie.genre.name));
// movies.map(movie => console.log(movie[sortBy]["name"]));
// movies.map(movie => console.log(movie[sortBy].name));

// const sortedMovies = movies.sort((prevMovie, nextMovie) => {
//   // console.log(prevMovie[sortBy]);
//   // if (prevMovie[sortBy] < nextMovie[sortBy]) return -1;
//   if (prevMovie[sortBy].name < nextMovie[sortBy].name) return -1;
//   return 1;
// });
// console.log(sortedMovies);
