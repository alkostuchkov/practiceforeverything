import React, {useState} from "react"
import {FaTrashAlt} from "react-icons/fa"

function Content() {
  const [items, setItems] = useState([
    {id: 1, checked: false, item: "Item 1"},
    {id: 2, checked: true, item: "Item 2"},
    {id: 3, checked: false, item: "Item 3"}
  ])

  function handleCheck(id) {
    const listItems = items.map(item =>
      item.id === id ? {...item, checked: !item.checked} : item
    )
    setItems(listItems)
  }

  return (
    <main>
      <ul>
        {items.map(item => (
          <li className="item" key={item.id}>
            <input
              type="checkbox"
              checked={item.checked}
              onChange={() => handleCheck(item.id)}
            />
            <label>{item.item}</label>
            <FaTrashAlt role="button" tabIndex="0" className="svg" />
          </li>
        ))}
      </ul>
    </main>
  )
}

export default Content
