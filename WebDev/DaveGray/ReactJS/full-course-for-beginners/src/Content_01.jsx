import React, {useState} from "react"

function Content() {
  const [name, setName] = useState(getName())
  const [count, setCount] = useState(0)

  function getName() {
    const names = ["Alexander", "John", "Maxim"]
    const nameIndex = Math.floor(Math.random() * names.length)

    return names[nameIndex]
  }

  function handelNameChange() {
    setName(getName())
  }

  function handleClick() {
    console.log("Clicked...")
    // console.log("count before setCount:", count)
    // setCount(prev => {
    //   console.log("In setCount: prev =", prev)
    //   console.log("In setCount: count =", count)
    //   return prev
    // })
    setCount(count + 1)
    setCount(count + 1)
    console.log("count after setCount:", count)
  }

  function handleClick_2() {
    console.log(count)
  }

  function handleClick2(name) {
    console.log(`It was clicked by ${name}.`)
  }

  function handleClick3(event) {
    console.log(event.target.textContent)
  }

  return (
    <main>
      {/* <p>Hello, {handelNameChange()}!</p> */}
      <p>Hello, {name}!</p>
      <button onClick={handelNameChange}>Change the name!</button>
      <button onClick={handleClick}>Click Me!</button>
      <button onClick={handleClick_2}>Click Me 2!</button>
      <button onClick={() => handleClick2(name)}>Click Me, {name}!</button>
      <button onClick={event => handleClick3(event)}>Event!</button>
    </main>
  )
}

export default Content
