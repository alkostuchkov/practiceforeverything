// jQuery("body").append("<a href='https://google.com'>Google</a>");

// console.log(document.getElementById("first_hello").innerText);
// document.getElementById("first_hello").innerText = "Hello, JavaScript!!!";

// // let classSelector = document.querySelector(".second_hello");
// let classSelector = document.querySelector("div.second_hello");
// console.log(classSelector);

class Car {
  constructor(speed) {
    this.speed = speed;
  }

  getSpeed() {
    return `My speed is ${this.speed} km/h.`;
  }

  setSpeed(speed) {
    this.speed = speed;
  }
}

let myCar = new Car(300);
document.querySelector("#my_car1").innerText = myCar.getSpeed();
myCar.setSpeed(500);
document.querySelector("#my_car2").innerText = myCar.getSpeed();
