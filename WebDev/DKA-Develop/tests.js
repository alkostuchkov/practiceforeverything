// jQuery("body").append("<a href='https://google.com'>Google</a>");

// let Car = {
//     model: "ford",
//     color: "#ff0000",
//     Audio: {
//         brand: "Sony",
//         speakers: 12
//     },
//     cost: 1e6
// };
// console.log(typeof Car);

// myCar = Car;
// // myCar.color = "grey";
// // myCar.color = "purple";
// myCar.color = "lime";
// console.log(myCar.model, myCar.color);
// console.log(myCar.Audio.brand);
// console.log(myCar.Audio);

// if (myCar.color == "green") {
//     console.log("Car's color is green");
// } else if (myCar.color == "lime") {
//     console.log("Car's color is lime");
// } else {
//     console.log("Car's color is unknown color");
// }

// // myCar.color == "grey" ? myCar.color = "lime" : console.log("myCar.color is not grey.");
// myCar.color = myCar.color == "grey" ? "lime" : "grey";
// console.log(myCar.color);

// if (myCar.cost >= 1000000) {
//     console.log("The myCar is expencive", myCar.cost)
// } else {
//     console.log("The myCar is NOT expencive", myCar.cost)
// }

// let car1 = Car;
// let car2 = Car;
// car1.color = "red";
// car2.color = "green";
// console.log("car1.color =", car1.color, "\ncar2.color =", car2.color)

// ==== Functions =====
// function showMessage(whose, message="hello!") {
//     console.log(whose, "says", message);
// }
// showMessage("Alex", "good bye!");

// --- Anonymous ---
// let anonymous = function (whose, message="hello!") {
//     console.log(whose, "says", message);
// }
// anonymous("Alex", "good bye!");

// (function (whose, message="hello!") {
//     console.log(whose, "says", message);
// })("Alex");

// let anonym = () => console.log("Anonymous function.");
// let anonym = () => "Anonymous function.";
// let anonym = () => {return "Anonymous function."};
// let anonym = () => {
//     let count = 2;
//     return "Anonymous function. " + count;
// }
// console.log(anonym());

// let newCar = {
//     color: "green",
//     // open: function () {
//     //     console.log("Opening...");
//     // }
//     open() {
//         console.log("Opening...");
//     }
// }
// newCar.open();

// let arr = [1, 2, 3, 77];
// for(let i = 0; i < arr.length; i++) {
//     console.log(i);
// }

// let hater = {
//     firstName: "Bill",
//     lastName: "Gates",
//     getName() {
//         return `${this.firstName} ${this.lastName}`;
//     }
// }
// console.log(hater.getName());
// let hater2 = {
//     firstName: "Donald",
//     lastName: "Duck",
//     getName() {
//         return `${this.firstName} ${this.lastName}`;
//     }
// }
// console.log(hater2.getName());

class Hater {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    sayName() {
        console.log(`Our hater is ${this.firstName} ${this.lastName}.`);
    }
}

let h1 = new Hater("Bill", "Gates");
h1.sayName();
new Hater("Donald", "Duck").sayName();



