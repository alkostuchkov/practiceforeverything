import React from "react";
import PostItem from "./PostItem";

// const PostList = function ({posts, title}) {
//   console.log(posts);
//   console.log({posts});
//   return (
//     <div>
//       <h1 style={{textAlign: "center"}}>{title}</h1>
//       {posts.map(post => (
//         <PostItem post={post} key={post.id} />
//       ))}
//     </div>
//   );
// };

const PostList = function (props) {
  // console.log(props);
  // console.log(props.posts);
  // console.log(props.title);
  return (
    <div>
      <h1 style={{textAlign: "center"}}>{props.title}</h1>
      {props.posts.map((post, index) => (
        <PostItem post={post} idx={index + 1} key={post.id} />
      ))}
    </div>
  );
};

export default PostList;
