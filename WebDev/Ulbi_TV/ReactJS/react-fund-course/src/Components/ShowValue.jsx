import React, {useState} from "react";

const ShowValue = function (props) {
  let [value, setValue] = useState("Just a text in the input");
  console.log(props);
  console.log({props});
  // console.log(props.value);
  // console.log({props}.props);

  return (
    <div>
      <h2>{value}</h2>
      <input
        type="text"
        value={value}
        onChange={event => {
          setValue(event.target.value);
        }}
      />
    </div>
  );
};

export default ShowValue;
