import React from "react";
import MyButton from "./ui/button/MyButton";

const PostItem = function (props) {
  return (
    <div className="post">
      <div className="post-content">
        <strong>
          {props.idx}. {props.post.title}
        </strong>
        <div>{props.post.body}</div>
      </div>
      <div className="post-btns">
        {/* <button>Delete</button> */}
        <MyButton>Delete</MyButton>
      </div>
    </div>
  );
};

export default PostItem;
