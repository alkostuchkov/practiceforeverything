import React, {useState} from "react";

const Counter = function () {
  let [counter, setCounter] = useState(0);

  function increment() {
    setCounter(++counter);
  }

  function decrement() {
    setCounter(--counter);
  }

  return (
    <div>
      <h1>{counter}</h1>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

export default Counter;
