import React from "react";

class CounterClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0
    };

    // this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this); // for function
  }

  increment = () => {
    // console.log(this); //in the arrow function 'this' points to this Class object!!!
    // let currentCounter = this.state.counter;
    // currentCounter++;
    // this.setState({counter: currentCounter});
    this.setState({counter: this.state.counter + 1});
  };

  decrement() {
    // console.log(this); //without this.decrement = this.decrement.bind(this) in consturctor 'this' in the decrement() is undefined!!!

    // let currentCounter = this.state.counter;
    // currentCounter--;
    // this.setState({counter: currentCounter});
    this.setState({counter: this.state.counter - 1});
  }

  render() {
    return (
      <div>
        <h1>{this.state.counter}</h1>
        <button onClick={this.increment}>Increment</button>
        <button onClick={this.decrement}>Decrement</button>
      </div>
    );
  }
}

export default CounterClass;
