import React from "react";
import classes from "./MyButton.module.css";

// const MyButton = props => {
const MyButton = ({children, ...props}) => {
  // console.log(props);
  // return <button className={classes.myBtn}>{props.children}</button>;
  return (
    <button {...props} className={classes.myBtn}>
      {children}
    </button>
  );
};

export default MyButton;
