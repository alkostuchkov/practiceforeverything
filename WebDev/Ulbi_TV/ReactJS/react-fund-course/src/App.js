import React, {useState} from "react";
// import React, {useRef, useState} from "react";
// import Counter from "./Components/Counter";
// import CounterClass from "./Components/CounterClass";
// import ShowValue from "./Components/ShowValue";
// import PostItem from "./Components/PostItem";
import "./css/App.css";
import PostList from "./Components/PostList";
import MyButton from "./Components/ui/button/MyButton";
import MyInput from "./Components/ui/input/MyInput";

function App() {
  let [posts, setPosts] = useState([
    {
      id: 1,
      title: "JavaScript",
      body: "JavaScript is a programming language."
    },
    {
      id: 2,
      title: "Python",
      body: "Python is a programming language."
    },
    {
      id: 3,
      title: "C++",
      body: "C++is a programming language."
    }
  ]);

  // let [posts2, setPosts2] = useState([
  //   {
  //     id: 1,
  //     title: "JavaScript",
  //     body: "JavaScript is a programming language."
  //   },3
  //     title: "Python",
  //     body: "Python is a programming language."
  //   },
  //   {
  //     id: 3,
  //     title: "C++",
  //     body: "C++is a programming language."
  //   }
  // ]);

  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  // const bodyInputRef = useRef();

  function addNewPost(event) {
    event.preventDefault();
    const newPost = {
      id: Date.now(),
      title,
      body
    };
    setPosts([...posts, newPost]);
    setTitle("");
    setBody("");
    // console.log(newPost);
    // console.log(title);
    // console.log(body);
    // console.log(bodyInputRef.current.value);
  }

  return (
    <div className="App">
      <form>
        {/* Управляемый компонент */}
        <MyInput
          type="text"
          placeholder="Post name"
          value={title}
          onChange={event => {
            setTitle(event.target.value);
          }}
        />
        {/* Управляемый компонент */}
        <MyInput
          type="text"
          placeholder="Post description"
          value={body}
          onChange={event => {
            setBody(event.target.value);
          }}
        />
        {/* Неуправляемый компонент */}
        {/* <MyInput
          type="text"
          placeholder="Post description"
          ref={bodyInputRef}
        /> */}
        <MyButton onClick={addNewPost}>Create post</MyButton>
      </form>
      <PostList posts={posts} title={title} />
      {/* <PostList posts={posts2} title="List 2 of posts" /> */}

      {/* <CounterClass /> */}
      {/* <ShowValue value="hello" obj={{name: "John", age: 13}} /> */}
      {/* <Counter /> */}
    </div>
  );
}

export default App;
